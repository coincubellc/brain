from decimal import Decimal as dec
import numpy as np
import pytest
import requests_mock
from database.database import (Balance, Cube, Currency, db_session,
                               Exchange, ExPair, Transaction, User)
from utils.reconcile import (cur_ids_from_balances, remove_delisted,
                             get_currency, add_new_balance, add_virgin_bals,
                             update_filled, reconcile_balances, reconcile_order)
from utils.order import (bals_from_order)
from utils.draft_transaction import draft_deposit_tx
from utils import (api)


def test_cur_ids_from_balances(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    assert cur_ids_from_balances(cube, ex) == [1,2,3,4,6]


def test_remove_delisted(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    # Delist an ex_pair
    ex_pair = ExPair.query.filter_by(
                        base_symbol='LTC', 
                        quote_symbol='BTC'
                        ).first()
    ex_pair.active = False
    db_session.add(ex_pair)
    db_session.commit()
    remove_delisted(cube, ex)
    bal = Balance.query.filter_by(
                        cube_id=cube.id,
                        currency_id=4,
                            ).first()
    assert bal == None


def test_get_currency(db_connection):
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cur = Currency.query.filter_by(symbol='ETH').first()
    assert get_currency(ex, 'ETH') == cur


def test_add_new_balance(db_connection):
    cube = db_connection
    cur = Currency.query.filter_by(symbol='DASH').first()
    ex = Exchange.query.filter_by(name='Bittrex').first()
    add_new_balance(cube, cur, ex, 100, 100, 100)
    db_session.refresh(cube)
    for bal in cube.balances:
        if bal.symbol == 'DASH':
            assert bal.symbol == 'DASH'


def test_add_virgin_bals(db_connection):
    cur = Currency.query.filter_by(symbol='ETC').first()
    bal = Balance.query.filter_by(currency_id=cur.id).first()
    # ETC balance shouldn't exist yet
    if bal:
        return False
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    add_virgin_bals(cube, ex)
    bal = Balance.query.filter_by(currency_id=cur.id).first()
    assert bal != None


def test_update_filled(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    order = {'filled': 0.4, 'avg_price': 0.003}
    order_id = 'a123x456'
    # Get starting balances
    start_base_bal, start_quote_bal = bals_from_order(cube, ex.id, order_id)
    start_base_bal_total = start_base_bal.total
    start_quote_bal_total = start_quote_bal.total

    if update_filled(cube, ex, order_id, order):
        end_base_bal, end_quote_bal = bals_from_order(cube, ex.id, order_id)
        # Assert that OpenOrder table has been updated with filled and avg_price
        assert round(cube.all_orders[order_id].filled, 8) == round(dec(order['filled']), 8)
        assert round(cube.all_orders[order_id].avg_price, 8) == round(dec(order['avg_price']), 8)
        # Assert that balances have been updated
        assert start_base_bal_total != end_base_bal.total
        assert start_quote_bal_total != end_quote_bal.total


def test_reconcile_balances(db_connection, monkeypatch):
    MOCK_EXAPI_URL = 'http://api.coincube.io'
    MOCK_METHOD_GET = 'GET'
    MOCK_EXCHANGE = 'Bittrex'
    MOCK_ENDPOINT = '/history'
    MOCK_PARAMS = { 'foo': 'bar' }

    monkeypatch.setattr(api, '_exapi_url', MOCK_EXAPI_URL)
    monkeypatch.setattr(api, 'GRACE_TIME', 1)
    monkeypatch.setattr(api, 'API_RETRIES', 1)

    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests: 
        response_json = [{'price': 0.032}]
        mock_requests.get(MOCK_URL, json=response_json)

        cube = db_connection
        ex = Exchange.query.filter_by(name='Bittrex').first()
        cur_ids = cur_ids_from_balances(cube, ex)
        # Draft deposit txs for all existing balances
        bals = {}
        for bal in cube.balances:
            bals[bal.currency_id] = {'total': bal.total}
            draft_deposit_tx(cube, ex, bal.currency_id, cur_ids, bals)

        bals = {'ETC': {'available': 100, 'total': 100}, # Add new balance
                'BTC': {'available': 1100, 'total': 1100}, # Convert asset
                'LTC': {'available': 2000, 'total': 2000},
                'USD': {'available': 100, 'total': 100},
                'DASH': {'available': 100, 'total': 100},
                'ETH': {'available': 6000, 'total': 6000},
                # Removed GNO to simulate delisted asset
                }

        reconcile_balances(cube, ex, bals)

        # Assert new balance added
        cur = Currency.query.filter_by(symbol='ETC').first()
        ex_pair = ExPair.query.filter_by(
                            exchange_id=ex.id,
                            base_currency_id=cur.id
                            ).first()
        new_bal = Balance.query.filter_by(
                            cube_id=cube.id,
                            currency_id=cur.id,
                            ).first()
        assert new_bal != None
        
        # Assert convert transaction made
        cur = Currency.query.filter_by(symbol='BTC').first()
        ex_pair = ExPair.query.filter_by(
                            exchange_id=ex.id,
                            base_currency_id=cur.id
                            ).first()
        con_trans = Transaction.query.filter_by(
                            cube_id=cube.id,
                            ex_pair_id=ex_pair.id,
                            type='convert',
                            ).first()
        assert float(con_trans.base_balance) == float(1100)
        assert float(con_trans.base_amount) == float(100.0)

        # Assert delisted asset is removed with convert tx
        cur = Currency.query.filter_by(symbol='GNO').first()
        ex_pair = ExPair.query.filter_by(
                            exchange_id=ex.id,
                            base_currency_id=cur.id
                            ).first()
        con_trans = Transaction.query.filter_by(
                            cube_id=cube.id,
                            ex_pair_id=ex_pair.id,
                            type='convert',
                            ).first()
        assert con_trans.base_balance == 0
        assert con_trans.base_amount == -3000

        # Assert deslited balance was removed
        removed_bal = Balance.query.filter_by(
                            cube_id=cube.id,
                            currency_id=cur.id
                            ).first()
        assert removed_bal == None


def test_reconcile_order(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    # Removed ex_pair
    ex_pair = ExPair.query.filter_by(
                        base_symbol='GNO', 
                        quote_symbol='BTC'
                        ).first()
    ex_pair.active = False
    db_session.add(ex_pair)
    db_session.commit()

    # New balances
    bals = {'BTC': {'available': 1100, 'total': 1100},
            'LTC': {'available': 2000, 'total': 2000},
            'USD': {'available': 100, 'total': 100},
            'DASH': {'available': 100, 'total': 100},
            'ETH': {'available': 6000, 'total': 6000},
            }

    # Test removed ex_pair order
    order = {'filled': 0.7, 'avg_price': 0.0000245}
    order_id = 'a123x457'

    reconcile_order(cube, ex, order_id, order, bals)

    # Assert that removed ex_pair order is removed
    assert order_id not in cube.all_orders

    # Test that transaction is drafted
    order = {'filled': 0.7, 'avg_price': 0.000245}
    order_id = 'a123x456'
    ex_pair = ExPair.query.filter_by(id=1).first()

    reconcile_order(cube, ex, order_id, order, bals)

    trans = Transaction.query.filter_by(
                    cube_id=cube.id,
                    ex_pair_id=ex_pair.id,
                    api_response=order_id,
                    ).first()

    assert trans.api_response == order_id

