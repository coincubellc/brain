import pytest
from decimal import Decimal as dec
from random import random
from database import (CubeCache, db_session, Exchange,
                      ExPair, log)
from trader import (place_orders, cancel_orders,
                    trigger_rebalance, order_reconciliation)
import trader
from tools import update_cube_cache
from utils import order, api
from .conftest import create_orders


@pytest.fixture
def place_order_setup(mocker):
    mocker.patch.object(order, 'api_request')
    mocker.patch.object(order, 'get_api_creds')

    order.api_request.return_value = f'a123x{round(random() * 100000)}'

    order.get_api_creds.return_value = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }


def test_place_orders(db_connection, place_order_setup):
    cube = db_connection
    update_cube_cache(cube.id, True)
    cache = CubeCache.query.filter_by(cube_id=cube.id).first()
    assert cache.processing == True
    # Orders
    # amount, price, ex_pair_id, side
    orders = [(10, 0.04, 3, 'buy'),
              (15, 0.1, 2, 'buy')]
    place_orders(cube.id, orders)
    # Assert cache is updated
    db_session.refresh(cache)
    assert cache.processing == False
    # Assert orders placed
    db_session.refresh(cube)
    assert len(cube.orders) == 4


@pytest.fixture
def cancel_order_setup(mocker):
    mocker.patch.object(order, 'api_request')
    mocker.patch.object(order, 'get_api_creds')

    order.api_request.return_value = True

    order.get_api_creds.return_value = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }


def test_cancel_orders(db_connection, cancel_order_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cancel = []
    for order in cube.orders:
        o = {}
        o['order_id'] = order.order_id
        o['base'] = order.ex_pair.base_currency.symbol
        o['quote'] = order.ex_pair.quote_currency.symbol
        cancel.append(o)
    cancel_orders(cube.id, ex, cancel)
    db_session.refresh(cube)
    # Assert orders canceled
    assert len(cube.orders) == 0


@pytest.fixture
def cancel_order_db_setup(mocker):
    mocker.patch.object(trader, 'cancel_order')
    mocker.patch.object(trader, 'update_cube_cache')


def test_cancel_order_db(db_connection, cancel_order_db_setup):
    cube = db_connection
    ex = Exchange.query.first()
    orders = []
    for order in cube.orders:
        _order = {}
        _order['order_id'] = order.order_id
        _order['base'] = order.ex_pair.base_currency.symbol
        _order['quote'] = order.ex_pair.quote_currency.symbol
        orders.append(_order)
    trader.cancel_orders(cube.id, ex, orders)
    # assert trader.cancel_order.call_count == 2
    assert trader.update_cube_cache.call_count == 1
    trader.update_cube_cache.assert_called_with(cube.id, False)


def test_trigger_rebalance(db_connection):
    cube = db_connection
    # Assert first balance has target
    for bal in cube.balances:
        assert bal.target == 10
    # Assert trigger rebalance removes targets
    trigger_rebalance(cube.id)
    db_session.refresh(cube)
    for bal in cube.balances:
        assert bal.target == None


@pytest.fixture
def trigger_rebalance_db_setup(mocker):
    mocker.patch.object(trader, 'db_session')
    mocker.patch.object(trader, 'update_cube_cache')
    trader.db_session.add.return_value = True
    trader.db_session.commit.return_value = True


def test_trigger_rebalance_db(db_connection,
                              trigger_rebalance_db_setup):
    cube = db_connection
    trader.trigger_rebalance(cube.id)
    assert trader.db_session.add.call_count == 1
    trader.db_session.add.assert_called_with(cube)
    assert trader.db_session.commit.call_count == 1


@pytest.fixture
def order_recon_setup(mocker):
    mocker.patch.object(trader, 'api_request')
    mocker.patch.object(order, 'api_request')

    order.api_request.return_value = True

    orders = {
        'a123x456':
        {'base': 'ETH', 'quote': 'BTC',
                 'side': 'buy', 'price': 0.1, 'amount': 1,
                 'filled': 1, 'unfilled': 0, 'avg_price': 0.1
         },
        'a123x457':
        {'base': 'GNO', 'quote': 'BTC',
                 'side': 'buy', 'price': 0.2, 'amount': 1,
                 'filled': 1, 'unfilled': 0, 'avg_price': 0.2
         },
        # Simulate local db order not on exchange
        'a123x459':
        {'base': 'DASH', 'quote': 'BTC',
                 'side': 'buy', 'price': 0.2, 'amount': 1,
                 'filled': 0, 'unfilled': 10, 'avg_price': 0
         }
    }

    trader.api_request.return_value = orders


@pytest.mark.skip(reason="no way of currently testing this")
def test_order_reconciliation(db_connection, order_recon_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    assert cube.unrecognized_activity == False

    # New balances
    bals = {'BTC': {'available': 999.7, 'total': 999.7},
            'LTC': {'available': 2000, 'total': 2000},
            'USD': {'available': 100, 'total': 100},
            'GNO': {'available': 3001, 'total': 3001},
            'ETH': {'available': 6001, 'total': 6001},
            }

    # Add back in orders
    orders = [{'ex_pair_id': 1, 'order_id': 'a123x456',
               'side': 'buy', 'price': 0.1, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0},
              {'ex_pair_id': 4, 'order_id': 'a123x457',
               'side': 'buy', 'price': 0.2, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0},
              # Simulate local db order not on exchange
              {'ex_pair_id': 4, 'order_id': 'a123x459',
               'side': 'buy', 'price': 0.2, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0}]
    create_orders(cube, orders)

    db_session.refresh(cube)
    creds = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }
    order_reconciliation(cube, ex, creds, bals)
    db_session.refresh(cube)

    # Assert rogue order deleted
    assert 'a123x458' not in cube.all_orders
    # Assert balances updated
    assert round(cube.balances[1].total, 8) == round(dec(999.7), 8)
    # Assert exchange orders canceled
    assert 'a123x457' not in cube.all_orders
    # Assert local db order missing from exchange deleted
    assert 'a123x459' not in cube.all_orders


@pytest.fixture
def order_recon_db_setup(mocker):
    mocker.patch.object(trader, 'api_request')
    mocker.patch.object(trader, 'reconcile_order')
    mocker.patch.object(trader, 'cancel_order')


def test_order_recon_db(db_connection, order_recon_db_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    bals = {'BTC': {'available': 999.7, 'total': 999.7},
            'LTC': {'available': 2000, 'total': 2000},
            'USD': {'available': 100, 'total': 100},
            'GNO': {'available': 3001, 'total': 3001},
            'ETH': {'available': 6001, 'total': 6001},
            }
    orders = [{'ex_pair_id': 1, 'order_id': 'a123x456',
               'side': 'buy', 'price': 0.1, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0},
              {'ex_pair_id': 4, 'order_id': 'a123x457',
               'side': 'buy', 'price': 0.2, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0},
              # Simulate local db order not on exchange
              {'ex_pair_id': 4, 'order_id': 'a123x459',
               'side': 'buy', 'price': 0.2, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0}]
    create_orders(cube, orders)
    db_session.refresh(cube)
    creds = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }

    trader.order_reconciliation(cube, ex, creds, bals)

    assert trader.api_request.call_count == 4
    assert trader.reconcile_order.call_count == 3
