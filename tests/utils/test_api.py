# Setup environments
from datetime import datetime
from collections import namedtuple
import pytest
import requests
import requests_mock
# import time
from decimal import Decimal
from utils import api
import database
from database import (Exchange, Connection, ConnectionError,
                      db_session)


MOCK_API_RETRIES = 5
MOCK_GRACE_TIME = 1
MOCK_EXAPI_URL = 'http://api.coincube.io'
MOCK_CUBE = 7331
MOCK_METHOD_GET = 'GET'
MOCK_METHOD_POST = 'POST'
MOCK_METHOD_DEL = 'DEL'
MOCK_METHOD_PUT = 'PUT'
MOCK_EXCHANGE = 'gdax'
MOCK_ENDPOINT = '/allocations'
MOCK_PARAMS = { 'foo': 'bar' }


@pytest.fixture()
def test_api_setup(monkeypatch, mocker):
    monkeypatch.setattr(api, '_exapi_url', MOCK_EXAPI_URL)
    monkeypatch.setattr(api, 'GRACE_TIME', MOCK_GRACE_TIME)
    monkeypatch.setattr(api, 'API_RETRIES', MOCK_API_RETRIES)

    # Patches for `api_requests`
    mocker.patch.object(api, 'sleep')
    mocker.patch.object(api, 'record_api_key_error')
    mocker.patch.object(api, 'fail_connection')
    mocker.patch.object(Exchange, 'query')

    api.sleep.return_value = None
    Exchange.query.filter_by.one.return_value = { 'exchange_mocked': True }


# NOTE: Don't remove this because I want to show it in a meeting
# @pytest.fixture()
# def get_api_creds_setup(mocker):
    # mocker.patch.object(Connection, 'query')
    # conn = Connection.query.filter_by(cube_id=cube.id, exchange_id=exchange.id).first()
    # Connection.query.filter_by.return_value.first.return_value\
    #     .exchange_id = 12
    # Connection.query.filter_by.return_value.first.return_value\
    #     .key = 'test key'
    # Connection.query.filter_by.return_value.first.return_value\
    #     .secret = 'test secret'
    # Connection.query.filter_by.return_value.first.return_value\
    #     .passphrase = 'test passphrase' 
    # Connection.query.filter_by.return_value.first.return_value\
    #     .decrypted_key = 'test key'
    # Connection.query.filter_by.return_value.first.return_value\
    #     .decrypted_secret = 'test secret'
    # Connection.query.filter_by.return_value.first.return_value\
    #     .decrypted_passphrase = 'test passphrase'


@pytest.fixture
def get_price_setup(monkeypatch, mocker):
    monkeypatch.setattr(api, '_price_cacher_url', 'http://price_cacher_url')


@pytest.fixture
def record_api_key_error_setup(mocker):
    mocker.patch.object(Exchange, 'query')
    mocker.patch.object(api, 'ConnectionError')
    mocker.patch.object(api, 'db_session')
    Exchange.query.filter_by.return_value.first.return_value.id = 1
 

@pytest.fixture
def fail_connection_setup(mocker):
    mocker.patch.object(api, 'delete_order')
    mocker.patch.object(api, 'db_session')
    api.db_session.add.return_value = None
    api.db_session.commit.return_value = None


@pytest.fixture
def delete_order_setup(mocker):
    mocker.patch.object(api, 'db_session')
    api.db_session.add.return_value = True
    api.db_session.commit.return_value = True


def test_delete_order(db_connection, delete_order_setup):
    cube = db_connection
    order_id = 'a123x456'

    api.delete_order(cube, order_id)
    assert api.db_session.add.call_count == 1
    api.db_session.add.assert_called_with(cube)
    assert api.db_session.commit.call_count == 1


def test_api_request_success(test_api_setup):
    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests:
        get_response_json = {'method': MOCK_METHOD_GET}
        post_response_json = {'method': MOCK_METHOD_POST}
        del_response_json = {'method': MOCK_METHOD_DEL}
        put_response_json = {'method': MOCK_METHOD_PUT}

        mock_requests.get(MOCK_URL, json=get_response_json)
        mock_requests.post(MOCK_URL, json=post_response_json)
        mock_requests.delete(MOCK_URL, json=del_response_json)
        mock_requests.put(MOCK_URL, json=put_response_json)

        get_response = api.api_request(MOCK_CUBE, MOCK_METHOD_GET,
            MOCK_EXCHANGE, MOCK_ENDPOINT, MOCK_PARAMS)

        post_response = api.api_request(MOCK_CUBE, MOCK_METHOD_POST,
            MOCK_EXCHANGE, MOCK_ENDPOINT, MOCK_PARAMS)

        del_response = api.api_request(MOCK_CUBE, MOCK_METHOD_DEL,
            MOCK_EXCHANGE, MOCK_ENDPOINT, MOCK_PARAMS)

        put_response = api.api_request(MOCK_CUBE, MOCK_METHOD_PUT,
            MOCK_EXCHANGE, MOCK_ENDPOINT, MOCK_PARAMS)

        assert get_response == get_response_json
        assert post_response == post_response_json
        assert del_response == del_response_json
        assert put_response == put_response_json


def test_api_request_exception(test_api_setup):
    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests:
        # Mock requests response to throw an exception
        requestsException = requests.exceptions.ConnectionError
        mock_requests.register_uri(MOCK_METHOD_GET, MOCK_URL,
            exc=requestsException)

        get_response_error = api.api_request(MOCK_CUBE,
                MOCK_METHOD_GET, MOCK_EXCHANGE, MOCK_ENDPOINT, MOCK_PARAMS)

        # If status code isn't 200 then `api_request` returns `None`
        assert get_response_error is None
            
        # Assert functions used in `api_request` get called the right 
        # amount of times
        assert api.record_api_key_error.call_count == (MOCK_API_RETRIES + 1)
        assert Exchange.query.filter_by.call_count == 1
        # assert api.fail_connection.call_count == 1
        assert api.sleep.call_count == MOCK_API_RETRIES

        # Assert functions are passed with the right params
        Exchange.query.filter_by.assert_called_with(name=MOCK_EXCHANGE)
        api.sleep.assert_called_with(MOCK_GRACE_TIME)
        # Note: These tests fail with the exception parameter for some 
        # reason, but the diff shows that is correct
        # api.record_api_key_error.assert_called_with(MOCK_CUBE,
        #     MOCK_EXCHANGE, requestsException)
        # api.fail_connection.assert_called_with(MOCK_CUBE, MOCK_EXCHANGE,
        #     requestsException)


def test_fail_connection(db_connection, fail_connection_setup):
    cube = db_connection
    order_id = 'a123x457'
    ex = Exchange.query.filter_by(name='Bittrex').first()

    api.fail_connection(cube, ex)

    assert api.delete_order.call_count == 1
    api.delete_order.assert_called_with(cube, order_id)
    api.db_session.add.call_count == 1
    api.db_session.add.assert_called_with(cube)
    api.db_session.commit.call_count == 1


def test_get_api_creds(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    api_creds = api.get_api_creds(cube, ex)

    expected = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase'
    }

    # `get_api_creds` should return the expected result if 
    # `echange_id` is 12
    assert api_creds == expected


def test_get_price(get_price_setup):
    EXCHANGE = 'gdax'
    BASE = 'base'
    QUOTE = 'quote'
    URL = f'http://price_cacher_url/{EXCHANGE}/{BASE}/{QUOTE}'

    with requests_mock.Mocker() as mock_requests:
        expected_response = {'price_str': '12'}
        mock_requests.get(URL, json=expected_response)
        get_price_response = api.get_price(EXCHANGE, BASE, QUOTE)

        # Should return the response as Decimal
        assert get_price_response == Decimal(expected_response['price_str'])

        # Should throw an exception on error
        with pytest.raises(Exception):
            requestsException = requests.exceptions.ConnectionError
            mock_requests.register_uri('GET', URL, exc=requestsException)
            get_price_with_exception = api.get_price(EXCHANGE, BASE, QUOTE)


def test_record_api_key_error(db_connection, record_api_key_error_setup):
    cube = db_connection
    conn = api.ConnectionError(
        user_id=cube.user_id,
        cube_id=cube.id,
        exchange_id=1,
        error_message=str('An error!'),      
    )

    api.record_api_key_error(cube, 'gdax', 'An error!')

    assert Exchange.query.filter_by.call_count == 1
    Exchange.query.filter_by.assert_called_with(name='gdax')

    assert api.db_session.add.call_count == 1
    api.db_session.add.assert_called_with(conn)

    assert api.db_session.commit.call_count == 1

    assert api.ConnectionError.call_count == 2
    api.ConnectionError.assert_called_with(
        user_id=cube.user_id,
        cube_id=cube.id,
        exchange_id=1,
        error_message=str('An error!'),      
    )
