from datetime import datetime
import numpy as np
import pytest
# from pytest_mock.mocker import call
from tools import cur_ids_from_balances
from database.database import (Algorithm, AssetAllocation, Balance, Cube, 
                               CombinedIndexPair1h, Connection, Currency, db, 
                               db_session, Exchange, ExPair, 
                               Focus, IndexPair, log, Order, Transaction, User)
from utils import allocations
from utils.allocations import (key_to_id, asset_composition, create_asset_allocs,
                               set_asset_allocation, set_allocations_to_index)
from utils.draft_transaction import draft_deposit_tx
from .conftest import (create_balances, create_deposit_txs, 
                       db_connection, populate_focus)


# This test doesn't mock the SQLAlchemy model, and test that the
# database functionality works correctly
def test_key_to_id(db_connection):
    assert key_to_id({ 'USD': 0, 'BTC': 0 }) == {1: 0, 2: 0}


@pytest.fixture
def test_key_to_id_setup_db(mocker):
    mocker.patch.object(Currency, 'query')
    class MockClass:
        id = 1
    Currency.query.filter_by.return_value.one.return_value = MockClass()


# This test make sure the SQLAlchemy model is querying with the 
# right arguments
def test_key_to_id_db(test_key_to_id_setup_db, mocker):
    key_to_id({ 'USD': 0, 'BTC': 0 })

    # Assert `filter_by` is called the right amount of time depending
    # of the expected result
    assert Currency.query.filter_by.call_count == 2
    # Assert `filter_by` calls with the expected params
    expected_calls = [
        mocker.call(symbol='USD'),
        mocker.call().one(),
        mocker.call(symbol='BTC'),
        mocker.call().one(),
    ]
    assert Currency.query.filter_by.mock_calls == expected_calls


def test_asset_composition(db_connection, monkeypatch):
    cube = db_connection
    Balance.query.filter_by(cube_id=cube.id).delete()
    db_session.refresh(cube)
    bals = [('BTC', 'Bittrex', 0.01),
            ('LTC', 'Bittrex', 1),
            ('ETH', 'Bittrex', 2),
            ('USD', 'Bittrex', 0),
            ('GNO', 'Bittrex', 4)]
    create_balances(cube, bals)
    db_session.refresh(cube)
    create_deposit_txs(cube, monkeypatch)
    assets, portfolio_balance = asset_composition(cube, 100)
    assert portfolio_balance > 0
    assert len(assets) <= round(portfolio_balance / 100)


@pytest.fixture
def asset_composition_db_setup(mocker):
    mocker.patch.object(ExPair, 'query')
    class MockClass:
        id = 1
    ExPair.query.filter_by.return_value.first.return_value = MockClass()


def test_asset_composition_db(db_connection, asset_composition_db_setup):
    cube = db_connection
    asset_composition(cube, 100)
    assert ExPair.query.filter_by.call_count == 1
    ExPair.query.filter_by.assert_called_with(exchange_id=1, active=True)


def test_create_asset_allocs(db_connection):
    cube = db_connection
    assets, portfolio_balance = asset_composition(cube, 100)
    asset_allocations = create_asset_allocs(assets, cube)
    log.debug(asset_allocations)
    # Assert equally weighted portfolio
    for asset in asset_allocations:
        assert asset_allocations[asset] == float(0.25)
    # Assert market cap weighted portfolio
    cube.risk_tolerance = 1
    db_session.add(cube)
    db_session.commit()
    db_session.refresh(cube)
    assets, port_bal = asset_composition(cube, 100)
    asset_allocations = create_asset_allocs(assets, cube)
    # Assert BTC allocation is correct (market cap weighted)
    assert asset_allocations[2] == float(0.8228723404255319)


@pytest.fixture
def set_asset_alloc_db_setup(mocker):
    mocker.patch.object(AssetAllocation, 'query')
    mocker.patch.object(allocations, 'db_session')

    class MockClass:
        percent = 5
        reserved_base = None
        reserved_quote = None

    AssetAllocation.query.filter_by.return_value.first\
        .return_value = MockClass()

    allocations.db_session.add.return_value = True
    allocations.db_session.commit.return_value = True


def test_set_asset_alloc_db(db_connection, set_asset_alloc_db_setup):
    cube = db_connection
    set_asset_allocation(cube, 1, 15)
    assert AssetAllocation.query.filter_by.call_count == 1
    AssetAllocation.query.filter_by.assert_called_with(
        cube_id=cube.id,
        currency_id=1,
    )
    asset = AssetAllocation.query.filter_by(
        cube_id=cube.id,
        currency_id=1,
    ).first()
    assert allocations.db_session.add.call_count == 1
    allocations.db_session.add.assert_called_with(asset)
    assert allocations.db_session.commit.call_count == 1


def test_set_asset_allocation(db_connection):
    cube = db_connection
    # Assert old asset allocations
    cur = Currency.query.get(2)
    allocation = AssetAllocation.query.filter_by(
                        cube_id=cube.id,
                        currency_id=cur.id
                        ).first()
    assert float(allocation.percent) == float(0.4)

    assets, port_bal = asset_composition(cube, 100)
    asset_allocations = create_asset_allocs(assets, cube)    
    # Update database with new asset allocations
    # Loop through asset allocations
    for asset_id, percent in asset_allocations.items():           
        set_asset_allocation(cube, asset_id, float(percent))
    # Assert allocations updated correctly in database 
    # (market cap weighted)
    db_session.refresh(allocation)
    assert round(float(allocation.percent), 4) == round(float(0.8228723404255319), 4)


@pytest.fixture
def setup_set_allocations_to_index_db(mocker):
    mocker.patch.object(allocations, 'create_asset_allocs')
    mocker.patch.object(allocations, 'set_asset_allocation')
    mocker.patch.object(allocations, 'db_session')
    allocations.db_session.add.return_value = True
    allocations.db_session.commit.return_value = True
    allocations.set_asset_allocation.return_value = True
    allocations.create_asset_allocs.return_value = {
        2: 0.125,
        3: 0.125,
        4: 0.125,
        5: 0.125,
        6: 0.125,
    }


def test_set_allocations_to_index_db(db_connection, setup_set_allocations_to_index_db):
    cube = db_connection
    assets, port_bal = asset_composition(cube, 50)
    set_allocations_to_index(cube)
    assert allocations.create_asset_allocs.call_count == 1
    allocations.create_asset_allocs.assert_called_with(assets, cube)

    assert allocations.set_asset_allocation.call_count == 6
    assert allocations.db_session.add.call_count == 2
    allocations.db_session.add.assert_called_with(cube)
    assert allocations.db_session.commit.call_count == 2


def test_set_allocations_to_index(db_connection):
    start_time = datetime.utcnow()
    cube = db_connection
    # Back to equally weighted portfolio
    cube.risk_tolerance = 10
    db_session.add(cube)
    db_session.commit()
    db_session.refresh(cube)
    # Delete old allocations
    for a in cube.allocations:
        db_session.delete(cube.allocations[a])
    db_session.commit()
    # Increase BTC balance
    for bal in cube.balances:
        if bal.currency_id == 2:
            bal.total = 3
            db_session.add(bal)
    db_session.commit()
    # New focus currencies
    currencies = Currency.query.all()
    # Remove USD
    del(currencies[0])
    populate_focus(currencies)
    set_allocations_to_index(cube)
    # Assert missing Dash balance added
    dash_bal = Balance.query.filter_by(currency_id=7).first()
    assert dash_bal.total == 0
    # Assert USD allocation added and set to 0%
    usd_alloc = AssetAllocation.query.filter_by(currency_id=1).first()
    assert usd_alloc.percent == 0
    # Assert BTC allocation set
    btc_alloc = AssetAllocation.query.filter_by(currency_id=2).first()
    assert round(float(btc_alloc.percent), 4) == round(float(0.142857142857), 4)
    # Assert cube.reallocated_at was updated
    db_session.refresh(cube)
    assert cube.reallocated_at > start_time


