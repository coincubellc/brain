import pytest
import requests_mock
import os, sys
from datetime import datetime
from decimal import Decimal as dec
import numpy as np
from ..config import (PROJECT_PATH, DATABASE_URI)

sys.path.append(PROJECT_PATH)
os.environ["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI


from database.database import (Algorithm, AssetAllocation, Balance, Cube, 
                               CombinedIndexPair1h, Connection, Currency, db, 
                               db_session, Exchange, ExPair, Focus,
                               IndexPair, log, Order, Transaction, User)
from utils.reconcile import (cur_ids_from_balances, remove_delisted,
                             get_currency, add_new_balance, add_virgin_bals,
                             update_filled, reconcile_balances, reconcile_order)
from utils import api
from utils.draft_transaction import draft_deposit_tx
from .test_api import test_api_setup


# Fixtures
def create_currencies(names):
    for name, m_cap in names:
        currency = Currency(symbol=name, name=name, market_cap=m_cap, cmc_id=0, base_protocol='')
        db_session.add(currency)

    db_session.commit()


def create_exchanges(names):
    for name in names:
        exchange = Exchange(name=name, active=True)
        db_session.add(exchange)

    db_session.commit()


def create_ex_pairs(pairs):
    for base, quote, ex in pairs:
        base = Currency.query.filter_by(symbol=base).first()
        quote = Currency.query.filter_by(symbol=quote).first()
        exchange = Exchange.query.filter_by(name=ex).first()

        p = ExPair(exchange_id=exchange.id, quote_currency_id=quote.id,
                   base_currency_id=base.id, quote_symbol=quote.symbol,
                   base_symbol=base.symbol, active=True)
        db_session.add(p)

    db_session.commit()


def create_index_pairs(ex_pairs):
    for pair in ex_pairs:
        i = IndexPair(
                quote_currency_id=pair.quote_currency_id,
                base_currency_id=pair.base_currency_id,
                quote_symbol=pair.quote_symbol,
                base_symbol=pair.base_symbol,
                active=True
                )
        db_session.add(i)
    db_session.commit()


def populate_index_1h_table(index_pairs):
    for pair in index_pairs:
        base_cur = Currency.query.filter_by(id=pair.base_currency_id).first()
        if base_cur.id == 2:
            # BTC, set to USD quote value
            close = 8000
        else:
            # Create decimal price for other assets (this simulates BTC as quote)
            close = 0.005
        c = CombinedIndexPair1h(
                timestamp=datetime.utcnow(),
                quote_currency_id=pair.quote_currency_id,
                base_currency_id=pair.base_currency_id,
                index_pair_id=pair.id,
                close=close
                )
        db_session.add(c)
    db_session.commit()


def create_allocations(cube, allocs, total=1):
    assert np.isclose(sum(d[1] for d in allocs), total)

    for sym, alloc in allocs:
        log.debug(sym)
        log.debug(alloc)
        c = Currency.query.filter_by(symbol=sym).first()
        a = AssetAllocation.query.filter_by(
                              cube_id=cube.id,
                              currency_id=c.id
                              ).first()
        if not a:
            a = AssetAllocation(cube_id=cube.id, currency_id=c.id,
                              percent=alloc, reserved_base=1, reserved_quote=1)
        else:
            a.percent = alloc
        db_session.add(a)

    db_session.commit()


def create_balances(cube, bals):
    for sym, ex, amount in bals:
        c = Currency.query.filter_by(symbol=sym).first()
        x = Exchange.query.filter_by(name=ex).first()
        b = Balance(cube_id=cube.id, currency_id=c.id, exchange_id=x.id,
                    available=0, total=amount, last=0, target=10)
        db_session.add(b)

    db_session.commit()


def create_orders(cube, orders):
    for order in orders:
        o = Order(
            cube_id=cube.id,
            ex_pair_id=order['ex_pair_id'],
            order_id=order['order_id'],
            side=order['side'],
            price=order['price'],
            amount=order['amount'],
            filled=order['filled'],
            unfilled=order['unfilled'],
            avg_price=order['avg_price']
            )
        db_session.add(o)

    db_session.commit()


def create_deposit_txs(cube, monkeypatch):
    MOCK_EXAPI_URL = 'http://api.coincube.io'
    MOCK_EXCHANGE = 'Bittrex'
    MOCK_ENDPOINT = '/history'

    monkeypatch.setattr(api, '_exapi_url', MOCK_EXAPI_URL)
    monkeypatch.setattr(api, 'GRACE_TIME', 1)
    monkeypatch.setattr(api, 'API_RETRIES', 1)

    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests: 
        response_json = [{'price': 8500}]
        mock_requests.get(MOCK_URL, json=response_json)

        ex = Exchange.query.filter_by(name='Bittrex').first()
        cur_ids = cur_ids_from_balances(cube, ex)
        # Draft deposit txs for all existing balances
        bals = {}
        for bal in cube.balances:
            bals[bal.currency_id] = {'total': bal.total}
            draft_deposit_tx(cube, ex, bal.currency_id, cur_ids, bals)


def create_focus():
    focus = Focus(
              type='Top Ten',
              count=10,
              )
    db_session.add(focus)
    db_session.commit()


def populate_focus(currencies):
    focus = Focus.query.filter_by(count=10).first()
    # Add currencies to focus
    focus.currencies = currencies
    db_session.add(focus)
    db_session.commit()


def create_connection(cube):

    exchange = Exchange.query.first()

    new_conn = Connection(user_id=cube.user_id, cube_id=cube.id,
                          exchange_id=exchange.id, key='key',
                          secret='secret', passphrase='passphrase',
                          failed_at=None)
    db_session.add(new_conn)
    db_session.commit()


def create_cube(kwargs=None):
    user = User(social_id="Test1", first_name="1", agreement='',
                otp_secret="x", cb_wallet_id="x", fiat_id=1)
    algo = Algorithm(name='test_algo')
    db_session.add(user)
    db_session.add(algo)
    db_session.commit()
    db_session.refresh(user)
    db_session.refresh(algo)
    dflt_args = dict(user_id=user.id, algorithm_id=algo.id, auto_rebalance=True,
                     closed_at=None, suspended_at=None, fiat_id=1, threshold=None,
                     rebalance_interval=None, balanced_at=None, reallocated_at=None,
                     risk_tolerance=10, focus_id=1, unrecognized_activity=False)
    if kwargs is not None:
        dflt_args.update(kwargs)

    cube = Cube(**dflt_args)

    log.debug(cube.algorithm)

    for i in [user, algo, cube]:
        db_session.add(i)

    db_session.commit()

    return cube


@pytest.fixture(scope='module')
def db_connection(request):
    db.drop_all()
    db.create_all()

    create_currencies([('USD', 0), ('BTC', 500000000000), 
                       ('ETH', 30000000000), ('LTC', 14000000000), 
                       ('ETC', 4000000000), ('GNO', 200000000), 
                       ('DASH', 10000000000), ('XRP', 20000000000)])

    create_exchanges(['Bittrex', 'Binance'])

    pairs = [('ETH', 'BTC', 'Bittrex'),
             ('LTC', 'BTC', 'Bittrex'),
             ('ETC', 'BTC', 'Bittrex'),
             ('GNO', 'BTC', 'Bittrex'),
             ('BTC', 'USD', 'Bittrex'),
             ('DASH', 'BTC', 'Bittrex'),
             ('XRP', 'BTC', 'Bittrex')]
    create_ex_pairs(pairs)

    ex_pairs = ExPair.query.all()
    create_index_pairs(ex_pairs)

    index_pairs = IndexPair.query.all()
    populate_index_1h_table(index_pairs)

    cube = create_cube()

    create_connection(cube)

    allocs = [('BTC', 0.40), ('ETH', 0.60)]

    create_allocations(cube, allocs)

    bals = [('BTC', 'Bittrex', 1000),
            ('LTC', 'Bittrex', 2000),
            ('ETH', 'Bittrex', 6000),
            ('USD', 'Bittrex', 100),
            ('GNO', 'Bittrex', 3000)]
    create_balances(cube, bals)

    orders = [{'ex_pair_id': 1, 'order_id': 'a123x456',
               'side': 'buy', 'price': 0.0024, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0},
              {'ex_pair_id': 4, 'order_id': 'a123x457',
               'side': 'buy', 'price': 0.00024, 'amount': 1,
               'filled': 0, 'unfilled': 1, 'avg_price': 0}]
    create_orders(cube, orders)

    currencies = Currency.query.all()
    create_focus()
    populate_focus(currencies)

    create_connection(cube)

    yield cube

    def teardown():
        db_session.close()
        db.drop_all()

    request.addfinalizer(teardown)

