import pytest
from decimal import Decimal
from database import (Order, Currency, Exchange)
from tools import (trunc, calc_indiv, calc_comb, liquidate)
from utils import order
from utils import api

@pytest.fixture
def add_new_order_setup(mocker):
    mocker.patch.object(order, 'db_session')


def test_add_new_order(db_connection, add_new_order_setup):
    cube = db_connection
    mock_order = {'ex_pair_id': 1, 'order_id': 'a123x456',
                  'side': 'buy', 'price': 0.0024, 'amount': 1,
                  'filled': 0, 'unfilled': 1, 'avg_price': 0}

    order.add_new_order(
        cube,
        mock_order['ex_pair_id'],
        mock_order['order_id'],
        mock_order['side'],
        mock_order['price'],
        mock_order['amount'],
    )

    # NOTE: Don't delete
    # new_order = Order(
    #     cube_id=cube,
    #     ex_pair_id=mock_order['ex_pair_id'],
    #     order_id=mock_order['order_id'],
    #     side=mock_order['side'],
    #     price=trunc(mock_order['price']),
    #     amount=trunc(mock_order['amount']),
    #     filled=0,
    #     unfilled=trunc(mock_order['amount']),
    #     avg_price=0,
    # )

    assert order.db_session.add.call_count == 1
    assert order.db_session.commit.call_count == 1
    # TODO: Throwing false even the diff shows the same, fix it.
    # order.db_session.add.assert_called_with(new_order)


def test_bals_from_order(db_connection):
    cube = db_connection
    order_id = 'a123x456'
    exchange_id = 1

    bal_base, bal_quote = order.bals_from_order(
        cube,
        exchange_id,
        order_id,
    )

    assert bal_base.total == Decimal(6000)
    assert isinstance(bal_base.currency, Currency)
    assert bal_quote.total == Decimal(1000)
    assert isinstance(bal_quote.currency, Currency)


@pytest.fixture
def cancel_order_success_setup(mocker):
    mocker.patch.object(order, 'get_api_creds')
    mocker.patch.object(order, 'api_request')
    mocker.patch.object(order, 'delete_order')

    order.get_api_creds.return_value = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }

    order.api_request.return_value = True
    order.delete_order.return_value = True


def test_cancel_order_success(db_connection, cancel_order_success_setup):
    cube = db_connection
    exchange_id = 1
    order_id = 'a123x456'

    response = order.cancel_order(cube.id, exchange_id, order_id,
                                  'mock_base', 'mock_quote')

    assert order.get_api_creds.call_count == 1
    assert order.api_request.call_count == 1
    assert order.delete_order.call_count == 1
    assert response == order_id


@pytest.fixture
def cancel_order_error_setup(mocker):
    mocker.patch.object(order, 'get_api_creds')
    mocker.patch.object(order, 'api_request')
    mocker.patch.object(order, 'delete_order')

    order.get_api_creds.return_value = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }

    order.api_request.return_value = False
    order.delete_order.return_value = True


def test_cancel_order_error(db_connection, cancel_order_error_setup):
    cube = db_connection
    exchange_id = 1
    order_id = 'a123x456'

    response = order.cancel_order(cube.id, exchange_id, order_id,
                                  'mock_base', 'mock_quote')

    assert order.get_api_creds.call_count == 1
    assert order.delete_order.call_count == 1
    assert order.api_request.call_count == 1


@pytest.fixture
def delete_order_setup(mocker):
    mocker.patch.object(order, 'db_session')
    order.db_session.add.return_value = True
    order.db_session.commit.return_value = True


def test_delete_order(db_connection, delete_order_setup):
    cube = db_connection
    order_id = 'a123x456'

    order.delete_order(cube, order_id)

    assert order.db_session.add.call_count == 1
    assert order.db_session.commit.call_count == 1



@pytest.fixture
def place_order_setup(mocker):
    mocker.patch.object(order, 'api_request')
    mocker.patch.object(order, 'get_api_creds')
    mocker.patch.object(order, 'add_new_order')

    order.api_request.return_value = 'a123x456'

    order.get_api_creds.return_value = {
        'key': 'key',
        'secret': 'secret',
        'passphrase': 'passphrase',
    }

    order.add_new_order.return_value.return_value = True

def test_place_order(db_connection, place_order_setup):
    cube = db_connection
    amount = 120
    price = 1000
    ex_pair_id = 1

    response = order.place_order(cube.id, ex_pair_id, 'buy', amount, price)

    assert response == 'a123x456'
    assert order.api_request.call_count == 1
    assert order.add_new_order.call_count == 1

    with pytest.raises(Exception):
        order.place_order(None, ex_pair_id, 'sell', amount, price)


@pytest.fixture
def target_orders_setup(mocker):
    mocker.patch.object(order, 'db_session')
    # mocker.patch.object(order, 'get_ex_pair')
    mocker.patch.object(order, 'api_request')

    order.db_session.add.return_value = True
    order.db_session.commit.return_value = True
    # order.get_ex_pair.return_value = 1, True
    order.api_request.return_value = {
        'min_amt': 23,
        'min_val': 22,
    }



def test_target_orders(db_connection, target_orders_setup):
    cube = db_connection
    full_indiv = calc_indiv(cube)
    indiv, _ = liquidate(cube, full_indiv)
    comb = calc_comb(cube, indiv)

    order.target_orders(cube, indiv, comb, orders=[])

    assert order.db_session.add.call_count == 2
    assert order.db_session.commit.call_count == 1
    assert order.api_request.call_count == 1