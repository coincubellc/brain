from decimal import Decimal as dec
import numpy as np
import pytest
import requests_mock
from database.database import (Algorithm, AssetAllocation, Balance, Cube, 
                               Currency, db, db_session, Exchange, ExPair, 
                               log, Order, Transaction, User)
from tools import cur_ids_from_balances
from utils import draft_transaction
from utils.draft_transaction import (draft_tx, draft_convert_tx,
                                     draft_deposit_tx)
from utils import (api)
from utils.reconcile import update_filled, reconcile_balances



# Tests
@pytest.fixture
def draft_tx_setup(mocker):
    mocker.patch.object(draft_transaction, 'db_session')
    draft_transaction.db_session.add.return_value = True
    draft_transaction.db_session.commit.return_value = True


def test_draft_tx(db_connection, draft_tx_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()

    # New balances
    bals = {'BTC': {'available': 999.9, 'total': 999.9},
            'LTC': {'available': 2000, 'total': 2000},
            'ETH': {'available': 6001, 'total': 6001},
            'USD': {'available': 100, 'total': 100},
            'DASH': {'available': 100, 'total': 100},
            }

    # Test that transaction is drafted correctly
    order = {'filled': 1, 'avg_price': 0.1}
    order_id = 'a123x456'
    ex_pair = ExPair.query.filter_by(id=1).first()

    # Update order info
    update_filled(cube, ex, order_id, order)
    # Draft transaction
    draft_tx(cube, ex, order_id, bals)

    trans = Transaction.query.filter_by(
                    cube_id=cube.id,
                    api_response=order_id,
                    ).first()
    # Assert base and quote balances match
    assert bals['BTC']['total'] == float(trans.quote_balance)
    assert bals['ETH']['total'] == float(trans.base_balance)
    assert draft_transaction.db_session.add.call_count == 3
    assert draft_transaction.db_session.commit.call_count == 1


def test_draft_deposit_tx(db_connection, monkeypatch):
    MOCK_EXAPI_URL = 'http://api.coincube.io'
    MOCK_EXCHANGE = 'Bittrex'
    MOCK_ENDPOINT = '/history'

    monkeypatch.setattr(api, '_exapi_url', MOCK_EXAPI_URL)
    monkeypatch.setattr(api, 'GRACE_TIME', 1)
    monkeypatch.setattr(api, 'API_RETRIES', 1)

    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests: 
        response_json = [{'price': 8500}]
        mock_requests.get(MOCK_URL, json=response_json)

        cube = db_connection
        ex = Exchange.query.filter_by(name='Bittrex').first()
        cur_ids = cur_ids_from_balances(cube, ex)

        # Draft deposit txs for all existing balances
        bals = {}
        for bal in cube.balances:
            bals[bal.currency_id] = {'total': bal.total}
            draft_deposit_tx(cube, ex, bal.currency_id, cur_ids, bals)

        all_trans = Transaction.query.filter_by(
                            cube_id=cube.id
                            ).all()
        # Deposit tx for XRP
        new_bals = {}
        cur = Currency.query.filter_by(symbol='XRP').first()
        ex_pair = ExPair.query.filter_by(base_symbol='XRP').first()
        
        new_bals[cur.id] = {'total': 10000}
        draft_deposit_tx(cube, ex, cur.id, cur_ids, new_bals)

        trans = Transaction.query.filter_by(
                        cube_id=cube.id,
                        ex_pair_id=ex_pair.id,
                        ).first()
        # Assert base and quote balances match
        assert float(trans.base_balance) == 10000
        assert float(trans.quote_balance) == float(999.9)


@pytest.fixture
def draft_convert_tx_db_setup(mocker):
    mocker.patch.object(draft_transaction, 'cur_ids_from_balances')
    mocker.patch.object(draft_transaction, 'api_request')
    mocker.patch.object(draft_transaction, 'db_session')
    draft_transaction.db_session.add.return_value = True
    draft_transaction.db_session.flush.return_value = True
    draft_transaction.db_session.commit.return_value = True
    draft_transaction.api_request.return_value = {
        0: {
            'price': 29,
        },
    }
    draft_transaction.cur_ids_from_balances\
        .return_value = [1, 2, 3, 4, 6]



# def test_draft_deposit_tx_db(db_connection):
def test_draft_convert_tx_db(db_connection, draft_convert_tx_db_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()

    for bal in cube.balances:
        draft_convert_tx(cube, ex, bal)

    assert draft_transaction.cur_ids_from_balances.call_count == 5
    assert draft_transaction.api_request.call_count == 5
    assert draft_transaction.db_session.add.call_count == 10
    assert draft_transaction.db_session.flush.call_count == 5
    assert draft_transaction.db_session.commit.call_count == 5


def test_draft_convert_tx(db_connection, monkeypatch):
    MOCK_EXAPI_URL = 'http://api.coincube.io'
    MOCK_EXCHANGE = 'Bittrex'
    MOCK_ENDPOINT = '/history'

    monkeypatch.setattr(api, '_exapi_url', MOCK_EXAPI_URL)
    monkeypatch.setattr(api, 'GRACE_TIME', 1)
    monkeypatch.setattr(api, 'API_RETRIES', 1)

    MOCK_URL = f'{MOCK_EXAPI_URL}/{MOCK_EXCHANGE}{MOCK_ENDPOINT}'

    with requests_mock.Mocker() as mock_requests: 
        response_json = [{'price': 8500}]
        mock_requests.get(MOCK_URL, json=response_json)

        cube = db_connection
        ex = Exchange.query.filter_by(name='Bittrex').first()
        # Get ExPair
        ex_pair = ExPair.query.filter_by(quote_symbol='USD').first()
        # New balances
        bals = {'BTC': {'available': 1100, 'total': 1100},
                'LTC': {'available': 2000, 'total': 2000},
                'ETH': {'available': 6001, 'total': 6001},
                'USD': {'available': 100, 'total': 100},
                }
        # Get BTC balance
        bal = Balance.query.filter_by(currency_id=2).first()
        # Assert starting BTC balance of 999.9
        assert float(bal.total) == float(999.9)

        # Update balance total
        bal.total = bals['BTC']['total']
        # Draft transaction
        draft_convert_tx(cube, ex, bal)

        trans = Transaction.query.filter_by(
                        cube_id=cube.id,
                        ex_pair_id=ex_pair.id,
                        ).order_by(
                        Transaction.created_at.desc()
                        ).first()
        # Assert base and quote balances match
        assert float(trans.base_balance) == 1100
        assert float(trans.quote_balance) == 100
        # Assert base and quote amounts match
        assert float(trans.base_amount) == float(100.1)
        assert(float(trans.quote_amount)) == 0


# TODO: Fix bug in draft_deposit_tx
@pytest.fixture
def draft_deposit_tx_db_setup(mocker):
    mocker.patch.object(draft_transaction, 'get_price')
    draft_transaction.get_price.return_value = 234


def test_draft_deposit_tx_db(db_connection, draft_deposit_tx_db_setup):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cur_ids = cur_ids_from_balances(cube, ex)

    # Draft deposit txs for all existing balances
    bals = {}
    for bal in cube.balances:
        bals[bal.currency_id] = {'total': bal.total}
        draft_deposit_tx(cube, ex, bal.currency_id, cur_ids, bals)
    db_session.refresh(cube)
    assert cube.transactions

