import pytest
import requests_mock
from decimal import Decimal as dec
from utils import api
from database import (Balance, CubeCache, Currency, db_session, 
                      Exchange, ExPair, log)
from tools import (trunc, cur_ids_from_balances,
                   update_cube_cache, get_ex_pair,
                   get_ex_pairs, add_new_balance,
                   add_or_update_balance, sanity_check,
                   calc_indiv, calc_comb)
from .conftest import create_allocations, create_balances


def test_trunc():
    assert trunc(0.13451265515121) == round(dec(0.13451265), 8)


def test_cur_ids_from_balances(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cur_ids = cur_ids_from_balances(cube, ex)
    log.debug(cur_ids)
    assert cur_ids == [1, 2, 3, 4, 6]


def test_update_cube_cache(db_connection):
    cube = db_connection
    update_cube_cache(cube.id, True)
    cache = CubeCache.query.filter_by(
                            cube_id=cube.id
                            ).first()
    assert cache.processing == True
    update_cube_cache(cube.id, False)
    db_session.refresh(cache)
    assert cache.processing == False


def test_get_ex_pair():
    ex = Exchange.query.filter_by(name='Bittrex').first()
    base = Currency.query.filter_by(symbol='BTC').first()
    quote = Currency.query.filter_by(symbol='USD').first()
    ex_pair, inverted = get_ex_pair(ex, base, quote)
    ep = ExPair.query.filter_by(base_symbol='BTC',
                                quote_symbol='USD').first()
    assert ep == ex_pair
    assert inverted == False


def test_get_ex_pairs(db_connection):
    exs = Exchange.query.filter_by(name='Bittrex')
    base = Currency.query.filter_by(symbol='BTC').first()
    ex_pairs = get_ex_pairs(exs, base)
    assert len(ex_pairs) == 7


def test_add_new_balance(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cur = Currency.query.filter_by(symbol='DASH').first()
    add_new_balance(cube, cur, ex, 10, 10, 10)
    bal = Balance.query.filter_by(
                exchange_id=ex.id, 
                currency_id=cur.id
                ).first()
    assert bal.total == 10


def test_add_or_update_balance(db_connection):
    cube = db_connection
    ex = Exchange.query.filter_by(name='Bittrex').first()
    cur = Currency.query.filter_by(symbol='DASH').first()
    add_or_update_balance(cube, cur, ex, 20, 20, 20)
    bal = Balance.query.filter_by(
                exchange_id=ex.id, 
                currency_id=cur.id
                ).first()
    assert bal.total == 20


def test_sanity_check(db_connection):
    cube = db_connection
    # Test normalization function
    # Create incorrect allocations (not summed to 1)
    allocs = [('BTC', 40), ('ETH', 60)]
    create_allocations(cube, allocs, total=100)
    db_session.refresh(cube)
    assert cube.allocations['BTC'].percent == 40
    sanity_check(cube)
    assert cube.allocations['BTC'].percent == round(dec(0.4), 11)
    # Assert that 0 balance cube is skipped
    Balance.query.filter_by(cube_id=cube.id).delete()
    db_session.refresh(cube)
    assert sanity_check(cube) == False
    # Assert that 0% allocations are added for balances
    # without allocations
    bals = [('BTC', 'Bittrex', 1000),
        ('LTC', 'Bittrex', 2000),
        ('ETH', 'Bittrex', 6000),
        ('USD', 'Bittrex', 100),
        ('GNO', 'Bittrex', 3000)]
    create_balances(cube, bals)
    db_session.refresh(cube)
    sanity_check(cube)
    db_session.refresh(cube)
    assert cube.allocations['LTC'].percent == 0


def test_calc_indiv(db_connection):
    cube = db_connection
    indiv = calc_indiv(cube)
    # Assert BTC value
    assert indiv.val.iloc[1] == 1000
    # Assert USD value
    assert indiv.val.iloc[0] == 0.0125


def test_calc_comb(db_connection):
    cube = db_connection
    indiv = calc_indiv(cube)
    comb = calc_comb(cube, indiv)
    # Assert BTC target value
    assert round(comb.val_tgt.iloc[1], 4) == 422.0050
    # Assert ETH target value
    assert round(comb.val_tgt.iloc[2], 4) == 633.0075

