import os

CELERY_ONCE = {
      'backend': 'celery_once.backends.Redis',
      'settings': {
        'url': os.getenv('CELERY_BROKER_URL'),
        'default_timeout': 60 * 60
      }
    }