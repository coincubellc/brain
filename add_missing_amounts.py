from database import db_session, Transaction
from sqlalchemy import or_


transactions = Transaction.query.filter(
                        or_(
                    Transaction.type == 'buy',
                    Transaction.type == 'sell'
                )).all()

for tx in transactions:
    if not tx._base_amount or not tx._quote_amount:
        tx.base_amount
        tx.quote_amount
    db_session.add(tx)
db_session.commit()