import builtins
builtins.test_mode = True  # must be before brain/models.* imports!

import unittest
from decimal import Decimal
import io
import os
from typing import List, Dict

from sqlalchemy.exc import OperationalError
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import pandas as pd
import numpy as np

# from models.database import SQLALCHEMY_DATABASE_URI, Base
# from models.base_models import Cube, User, Algorithm, Currency, Balance, Exchange, AssetAllocation, ExPair
from database import *
from tools import calc_indiv
import utils.regression
import tools
from optimizer import UseRegression, calculate_transfers

#####################################################################
# the test database should have 'test' in it's name
# THIS SHOULD NOT BE REMOVED, OTHERWISE A DATABASE THAT'S NOT
# THE TEST DATABASE MAY BE USED, WHICH COULD RESULT IN LOSS OF ALL DATA
# assert 'test' in SQLALCHEMY_DATABASE_URI
#####################################################################

TEST_DB_URI = os.getenv('TEST_DATABASE_URI')

# patch get_price
tools.get_price = lambda x, y, z: Decimal(1)

USD = None


def create_currencies(names: List[str], session) -> Dict[str, Currency]:
    r = {}
    for name in names:
        c = Currency(symbol=name, name=name, market_cap=1000000,
                     cmc_id=0, base_protocol='')
        r[name] = c
        session.add(c)

    session.commit()
    return r


def create_exchanges(names: List[str], session) -> Dict[str, Exchange]:
    r = {}
    for name in names:
        x = Exchange(name=name, active=True)
        r[name] = x
        session.add(x)

    session.commit()
    return r


def create_balances(data: List, cube, currencies, exchanges, session):
    r = []
    for sym, xc, amount in data:
        c = currencies[sym]
        x = exchanges[xc]
        b = Balance(cube_id=cube.id, currency_id=c.id, exchange_id=x.id,
                    available=0, total=amount, last=0, target=0)
        r.append(b)
        session.add(b)

    session.commit()
    return r


def create_allocations(data: List, cube, currencies, session):
    assert np.isclose(sum(d[1] for d in data), 100)

    r = []
    for sym, alloc in data:
        c = currencies[sym]
        a = AssetAllocation(cube_id=cube.id, currency_id=c.id,
                            percent=alloc, reserved_base=1, reserved_quote=1)
        r.append(a)
        session.add(a)

    session.commit()
    return r


def create_pairs(data: List, currencies, exchanges, session) -> Dict[str, ExPair]:
    r = []
    for base, quote, xc in data:
        base = currencies[base]
        quote = currencies[quote]
        xc = exchanges[xc]

        p = ExPair(exchange_id=xc.id, quote_currency_id=quote.id,
                   base_currency_id=base.id, quote_symbol=quote.symbol,
                   base_symbol=base.symbol, active=True)
        r.append(p)
        session.add(p)

    session.commit()
    return r


def create_cube(session, kwargs=None):
    user = User(social_id="Test1", first_name="1", agreement='',
                otp_secret="x", cb_wallet_id="x")
    algo = Algorithm(name='test_algo')

    dflt_args = dict(user_id=user.id, algorithm_id=algo.id, auto_rebalance=True,
                     closed_at=None, suspended_at=None, fiat_id=USD.id, threshold=None,
                     rebalance_interval=None, balanced_at=None, reallocated_at=None,
                     risk_tolerance=1, focus_id=None, 
                     unrecognized_activity=False)
    if kwargs is not None:
        dflt_args.update(kwargs)

    cube = Cube(**dflt_args)
    # print("creating cube, ", cube.id)

    for i in [user, algo, cube]:
        session.add(i)

    session.commit()
    return cube


class CubeTests(unittest.TestCase):
    def setUp(self):
        global cube, user, algo, USD, ETH, GDAX, BNC, BTC

        self.engine = create_engine(TEST_DB_URI, convert_unicode=True,
                                    pool_size=20, max_overflow=100, pool_recycle=280)
        try:
            Base.metadata.create_all(bind=self.engine)
        except OperationalError:
            tmp_engine = create_engine('mysql://test_user:testing@mysql', convert_unicode=True,
                                       pool_size=20, max_overflow=100, pool_recycle=280)
            conn = tmp_engine.connect()
            conn.execute("create database coincube_test")
            conn.close()

            self.engine = create_engine(TEST_DB_URI, convert_unicode=True,
                                        pool_size=20, max_overflow=100, pool_recycle=280)

            Base.metadata.create_all(bind=self.engine)

        self.session = scoped_session(sessionmaker(autocommit=False,
                                                   autoflush=False,
                                                   expire_on_commit=False,
                                                   bind=self.engine,
                                                   ))
        tools.db_session = self.session
        utils.regression.db_session = self.session
        Base.query = self.session.query_property()

        # currencies
        USD = Currency(symbol='USD', name='USD', market_cap=1000000, cmc_id=0, base_protocol='')

    def tearDown(self):
        self.session.close()
        self.connection = self.engine.connect()
        self.connection.execute("drop database coincube_test")
        self.connection.close()


class ToolsTests(CubeTests):
    def test_calc_comb(self):

        create_cube(self.session)
        cube = Cube.query.one()

        exchanges = ['GDAX', 'Binance']
        exchanges = create_exchanges(exchanges, self.session)
        currencies = ['BTC', 'ETH']
        currencies = create_currencies(currencies, self.session)

        pairs = [('ETH', 'BTC', 'GDAX'),
                 ('ETH', 'BTC', 'Binance')]

        create_pairs(pairs, currencies, exchanges, self.session)

        bals = [('BTC', 'GDAX', 1000),
                ('BTC', 'Binance', 1000),
                ('ETH', 'GDAX', 6000),
                ('ETH', 'Binance', 6000)]
        create_balances(bals, cube, currencies, exchanges, self.session)

        allocs = [('BTC', 40), ('ETH', 60)]
        create_allocations(allocs, cube, currencies, self.session)

        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)
        for i in range(len(comb)):
            if comb.iloc[i].cur.id == currencies['BTC'].id:
                self.assertAlmostEqual(comb.iloc[i].pct_tgt, 0.4)

            if comb.iloc[i].cur.id == currencies['ETH'].id:
                self.assertAlmostEqual(comb.iloc[i].pct_tgt, 0.6)


class OptimizerTests(CubeTests):
    def test_calculate_transfers(self):
        df_str = """cur_id	ex_id	val	price
BCH	Poloniex	10.194508009153319	1
BTC	Binance	15.934558493948902	1
BTC	Gemini	5.739130434782608	1
BTC	Poloniex	3.5011441647597255	1
ETH	Binance	3.4289556252801434	1
ETH	Gemini	16.0	1
ETH	Poloniex	10.091533180778033	1
LTC	Binance	19.76692066337965	1
LTC	Poloniex	8.752860411899313	1
XRP	Poloniex	6.590389016018307	1
"""
        df_str = io.StringIO(df_str)
        df = pd.read_csv(df_str, sep='\t', skiprows=0)

        # make a cube
        create_cube(self.session)
        cube = Cube.query.one()
        exchanges = list(df.ex_id.unique()) + ['External']
        exchanges = create_exchanges(exchanges, self.session)
        currencies = create_currencies(df.cur_id.unique(), self.session)

        pairs = [(r.cur_id, 'BTC', r.ex_id)
                 for _, r in df.iterrows()]
        create_pairs(pairs, currencies, exchanges, self.session)

        # add balances, allocs, pairs
        allocs = [(cur, 20) for cur in df.cur_id.unique()]
        create_allocations(allocs, cube, currencies, self.session)

        bals = [(r.cur_id, r.ex_id, r.val) for _, r in df.iterrows()]
        create_balances(bals, cube, currencies, exchanges, self.session)

        # build indiv, comb
        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)

        sol_indiv, sol_comb, transfer_details = utils.regression.regression(cube, indiv, comb)
        result = calculate_transfers(sol_indiv)
        buy_amounts, transfers, sell_amounts = result
        ex_start = sol_indiv.groupby('ex_id').sum().val
        ex_target = sol_indiv.groupby('ex_id').sum().val_nnls
        ex_diff = ex_start - ex_target

        for _, row in transfers.iterrows():
            ex_start[row.source_id] -= row.amount
            ex_start[row.dest_id] += row.amount

        for ex_id, v in zip(ex_start.index, ex_start):
            self.assertAlmostEqual(ex_target[ex_id], v)

        for ex_id, v in zip(ex_diff.index, ex_diff):
            tr_size = abs(transfers[transfers.source_id == ex_id].amount.sum()
                          + transfers[transfers.dest_id == ex_id].amount.sum())
            self.assertAlmostEqual(tr_size, abs(v))


class SolverTests(CubeTests):
    def test_solve_L1(self):
        df_str = """cur_id	ex_id	val	price
BCH	Poloniex	1.5806451612903225	1
BTC	Gemini	26.470588235294116	1
BTC	Poloniex	17.387096774193548	1
ETH	Gemini	3.5294117647058822	1
ETH	Poloniex	17.838709677419356	1
LTC	Poloniex	11.516129032258064	1
XRP	Poloniex	21.677419354838708	1
"""
        df_str = io.StringIO(df_str)
        df = pd.read_csv(df_str, sep='\t', skiprows=0)

        # make a cube
        create_cube(self.session)
        cube = Cube.query.one()
        exchanges = list(df.ex_id.unique()) + ['External']
        exchanges = create_exchanges(exchanges, self.session)
        currencies = create_currencies(df.cur_id.unique(), self.session)

        pairs = [(r.cur_id, 'BTC', r.ex_id)
                 for _, r in df.iterrows()]
        create_pairs(pairs, currencies, exchanges, self.session)

        # add balances, allocs, pairs
        allocs = [(cur, 20) for cur in df.cur_id.unique()]
        create_allocations(allocs, cube, currencies, self.session)

        bals = [(r.cur_id, r.ex_id, r.val) for _, r in df.iterrows()]
        create_balances(bals, cube, currencies, exchanges, self.session)

        # build indiv, comb
        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)

        ## solve & verify
        for use_reg in list(UseRegression):
            sol_indiv, sol_comb, transfer_details = utils.regression.regression(cube, indiv, comb, use_regression=use_reg)
            self.check_solution(cube, sol_indiv, indiv, sol_comb, ex_constraints=True)

    def test_solve_L1_with_external(self):
        df_str = """cur_id	ex_id	val	price
BCH	Binance	8.541666666666668	1
BCH	HitBTC	6.9230769230769225	1
BTC	Binance	11.875	1
BTC	External	1.0964912280701753	1
BTC	HitBTC	3.974358974358974	1
ETH	Binance	16.041666666666668	1
ETH	External	4.035087719298245	1
ETH	HitBTC	10.256410256410255	1
LTC	Binance	5.208333333333334	1
LTC	External	3.201754385964912	1
LTC	HitBTC	2.051282051282051	1
XRP	Binance	16.666666666666668	1
XRP	HitBTC	10.128205128205128	1
"""
        df_str = io.StringIO(df_str)
        df = pd.read_csv(df_str, sep='\t', skiprows=0)

        # make a cube
        create_cube(self.session)
        cube = Cube.query.one()
        exchanges = list(df.ex_id.unique())
        exchanges = create_exchanges(exchanges, self.session)
        currencies = create_currencies(df.cur_id.unique(), self.session)

        pairs = [(r.cur_id, 'BTC', r.ex_id)
                 for _, r in df.iterrows()]
        create_pairs(pairs, currencies, exchanges, self.session)

        # add balances, allocs, pairs
        allocs = [(cur, 20) for cur in df.cur_id.unique()]
        create_allocations(allocs, cube, currencies, self.session)

        bals = [(r.cur_id, r.ex_id, r.val) for _, r in df.iterrows()]
        create_balances(bals, cube, currencies, exchanges, self.session)

        # build indiv, comb
        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)

        ## solve & verify
        for use_reg in list(UseRegression):
            sol_indiv, sol_comb, transfer_details = utils.regression.regression(cube, indiv, comb, use_regression=use_reg)
            self.check_solution(cube, sol_indiv, indiv, sol_comb, ex_constraints=True)

    def check_solution(self, cube: Cube, sol_df: pd.DataFrame, indiv: pd.DataFrame,
                       comb: pd.DataFrame, ex_constraints=True):
        for val in sol_df.val_nnls:
            self.assertTrue(val >= 0)

        indiv_idx = {(indiv.reset_index().cur_id.iloc[i], indiv.reset_index().ex_id.iloc[i])
                     for i in range(len(indiv))}

        sol_idx = {(sol_df.reset_index().cur_id.iloc[i], sol_df.reset_index().ex_id.iloc[i])
                   for i in range(len(indiv))}

        diff = indiv_idx.symmetric_difference(sol_idx)

        self.assertFalse(diff)

        for i in indiv.index:
            for col in indiv.columns:
                if col == 'bal_tgt':
                    continue
                self.assertAlmostEqual(indiv.loc[i, col], sol_df.loc[i, col])

        self.assertAlmostEqual(sol_df.val_nnls.sum(), indiv.val.sum())

        self.assertFalse(pd.isnull(sol_df).any().any())

        if ex_constraints:
            sum_indiv = indiv.groupby('ex_id').val.sum()
            sum_sol = sol_df.groupby('ex_id').val_nnls.sum()
            for x in sum_indiv.index.unique():
                self.assertAlmostEqual(sum_indiv.loc[x], sum_sol[x])

            # check that external hasn't changed
            ext_id = Exchange.query.filter_by(name='External').one().id

            for i in indiv.index:
                if i[1] == ext_id:
                    self.assertAlmostEqual(indiv.loc[i, 'val'],
                                           sol_df.loc[i, 'val_nnls'])

        sum_sol = sol_df.groupby('cur_id').val_nnls.sum()
        for x in sum_sol.index.unique():
            self.assertAlmostEqual(sum_sol.loc[x], 20)

        # check comb
        for val in comb.val_nnls:
            self.assertAlmostEqual(val, 20)

        for val in comb.pct_nnls:
            self.assertAlmostEqual(val, 0.2)

        # check balances
        bals = {(b.currency_id, b.exchange_id): b for b in cube.balances}
        for _, row in sol_df.reset_index().iterrows():
            bal = bals[(row.cur_id, row.ex_id)]
            self.assertAlmostEqual(bal.target, row.bal_tgt)

    def test_solve_L2(self):
        df_str = """cur_id	ex_id	val	price
BCH	Poloniex	10.194508009153319	1
BTC	Binance	15.934558493948902	1
BTC	Gemini	5.739130434782608	1
BTC	Poloniex	3.5011441647597255	1
ETH	Binance	3.4289556252801434	1
ETH	Gemini	16.0	1
ETH	Poloniex	10.091533180778033	1
LTC	Binance	19.76692066337965	1
LTC	Poloniex	8.752860411899313	1
XRP	Poloniex	6.590389016018307	1
"""
        df_str = io.StringIO(df_str)
        df = pd.read_csv(df_str, sep='\t', skiprows=0)

        # make a cube
        create_cube(self.session)
        cube = Cube.query.one()
        exchanges = list(df.ex_id.unique()) + ['External']
        exchanges = create_exchanges(exchanges, self.session)
        currencies = create_currencies(df.cur_id.unique(), self.session)

        pairs = [(r.cur_id, 'BTC', r.ex_id) for _, r in df.iterrows()]
        create_pairs(pairs, currencies, exchanges, self.session)

        # add balances, allocs, pairs
        allocs = [(cur, 20) for cur in df.cur_id.unique()]
        create_allocations(allocs, cube, currencies, self.session)

        bals = [(r.cur_id, r.ex_id, r.val) for _, r in df.iterrows()]
        create_balances(bals, cube, currencies, exchanges, self.session)

        # build indiv, comb
        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)

        ## solve & verify
        for use_reg in list(UseRegression):
            sol_indiv, sol_comb, transfer_details = utils.regression.regression(cube, indiv, comb, use_regression=use_reg)
            self.check_solution(cube, sol_indiv, indiv, sol_comb, ex_constraints=False)

    def test_solve_L2_with_external(self):
        df_str = """cur_id	ex_id	val	price
BCH	Poloniex	10.194508009153319	1
BTC	External	15.934558493948902	1
BTC	Gemini	5.739130434782608	1
BTC	Poloniex	3.5011441647597255	1
ETH	External	3.4289556252801434	1
ETH	Gemini	16.0	1
ETH	Poloniex	10.091533180778033	1
LTC	External	19.76692066337965	1
LTC	Poloniex	8.752860411899313	1
XRP	Poloniex	6.590389016018307	1
"""
        df_str = io.StringIO(df_str)
        df = pd.read_csv(df_str, sep='\t', skiprows=0)

        # make a cube
        create_cube(self.session)
        cube = Cube.query.one()
        exchanges = list(df.ex_id.unique())
        exchanges = create_exchanges(exchanges, self.session)
        currencies = create_currencies(df.cur_id.unique(), self.session)

        pairs = [(r.cur_id, 'BTC', r.ex_id) for _, r in df.iterrows()]
        create_pairs(pairs, currencies, exchanges, self.session)

        # add balances, allocs, pairs
        allocs = [(cur, 20) for cur in df.cur_id.unique()]
        create_allocations(allocs, cube, currencies, self.session)

        bals = [(r.cur_id, r.ex_id, r.val) for _, r in df.iterrows()]
        create_balances(bals, cube, currencies, exchanges, self.session)

        # build indiv, comb
        indiv = calc_indiv(cube)
        comb = tools.calc_comb(cube, indiv)

        ## solve & verify
        for use_reg in list(UseRegression):
            sol_indiv, sol_comb, transfer_details = utils.regression.regression(cube, indiv, comb, use_regression=use_reg)
            self.check_solution(cube, sol_indiv, indiv, sol_comb, ex_constraints=False)


if __name__ == '__main__':
    unittest.main()
