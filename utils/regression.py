from optimizer import solve_allocations, calculate_transfers
from database import *


def regression(cube: Cube, indiv, comb, **kwargs):
    # perform non-negative linear regression to determine individual vals
    # set individual balance targets in db
    # external balances have targets set to None

    ext_id = Exchange.query.filter_by(name='External').one().id

    indiv_sol, comb_sol = solve_allocations(indiv, comb, cube.val_cur.id,
                                            ext_id, **kwargs)
    if indiv_sol is not None:
        log.info("Found L1 solution.")
        cube.requires_exchange_transfer = False
        indiv, comb = indiv_sol, comb_sol
    else:
        # try to solve without exchange constraints
        cube.requires_exchange_transfer = True
        indiv, comb = solve_allocations(indiv, comb, cube.val_cur.id,
                                        ext_id, L1=False, **kwargs)
        if indiv is not None:
            log.info("Found L2 solution.")
        else:
            log.info("No solution.")
            return None, None

    # enforce val_nnls = val for external currencies
    indiv = indiv.set_index('cur_id')

    # set unallocated currency targets to 0 if not external
    index = (comb.pct_tgt == 0) & (indiv.ex_id != ext_id)
    indiv.reset_index(inplace=True)  # prevent pandas duplicate index error
    indiv.loc[index.values, 'val_nnls'] = 0

    ### To do:
    # Should we use combined or individual price?
    # calculate balance targets using combined prices
    # indiv['comb_price'] = comb.price
    # indiv['bal_tgt'] = indiv.val_nnls  / indiv.comb_price
    # use indiv prices instead
    indiv['bal_tgt'] = indiv.val_nnls / indiv.price

    indiv = indiv.reset_index().set_index(['cur_id', 'ex_id'])

    # calculate combined nnls
    comb['val_nnls'] = indiv.groupby(level='cur_id').val_nnls.sum()
    comb['pct_nnls'] = comb.val_nnls / comb.val_nnls.sum()

    # logging
    log.debug('%s Individual valuations\n%s' %
              (cube, indiv.reset_index().loc[:, ['ex_id', 'cur_id', 'val', 'val_nnls']]))
    log.debug('%s Combined valuations\n%s' %
              (cube, comb.loc[:, ['cur', 'val', 'val_tgt', 'val_nnls', 'pct_tgt', 'pct_nnls']]))
    log.debug('%s Total valuations\n%s' %
              (cube, comb.loc[:, ['val', 'val_tgt', 'val_nnls', 'pct_tgt', 'pct_nnls']].sum()))

    set_target_balances(cube, indiv)
    if cube.requires_exchange_transfer:
        buy_amounts, transfers, sell_amounts = calculate_transfers(indiv)
        transfer_details = {
            'buy_amounts': buy_amounts,
            'transfers': transfers,
            'sell_amounts': sell_amounts,
        }
    else:
        transfer_details = {}

    return indiv, comb, transfer_details


def set_target_balances(cube, indiv):
    # set balance targets
    for b in cube.balances:
        try:
            b.target = float(indiv['bal_tgt'][b.currency_id, b.exchange_id])
            # convert nan to None
            if b.target != b.target:
                b.target = 0
            if b.target == float("inf"):
                b.target = 0
        except KeyError:
            b.target = None

        db_session.add(b)

    db_session.add(cube)
    db_session.commit()