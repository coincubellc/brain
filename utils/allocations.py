from database import *


def key_to_id(asset_allocations):
    allocations = {}
    for symbol in asset_allocations.keys():
        asset = Currency.query.filter_by(symbol=symbol).one()
        allocations[asset.id] = asset_allocations[symbol]
    return allocations


def asset_composition(cube, min_per_alloc):
    assets = []
    if cube.focus:
        log.debug(cube.focus)
        assets = cube.focus.sorted_currencies
        assets.reverse()
    # Check to see if assets are available on connected exchanges
    supported_assets = []
    for conn in cube.connections.values():
        ex_pair = ExPair.query.filter_by(
            exchange_id=conn.exchange.id,
            active=True
        )
        # Look for asset in either base or quote of ex_pair
        for asset in assets:
            cur1 = ex_pair.filter_by(quote_currency_id=asset.id).first()
            cur2 = ex_pair.filter_by(base_currency_id=asset.id).first()
            # If both return as None, continue
            if cur1 == None and cur2 == None:
                continue
            # Otherwise, append to supported_assets if in balances
            else:
                # Check for GDAX and only add supported assets 
                # based on balances (GDAX lists balances as 0 for supported)
                if conn.exchange.id == 2:
                    for bal in cube.balances:
                        if bal.currency == asset:
                            supported_assets.append(asset)
                else:
                    supported_assets.append(asset)

    assets = supported_assets
    # Portfolio Metrics
    portfolio_balance = round(cube.valuations()['val_fiat'], 2)
    # Find maximum number of assets that can be safely traded
    max_assets = round(portfolio_balance / min_per_alloc)
    # Remove excess assets
    if max_assets < len(assets):
        assets = assets[:max_assets]
    return assets, portfolio_balance


def create_asset_allocs(assets, cube):
    # Market capitalization of assets
    # and Equally Weighted allocations
    fiat_sym = cube.user.fiat.symbol
    total_cap = 0
    market_cap = {}
    equal_w = {}
    for asset in assets:
        if asset.symbol != fiat_sym:
            total_cap += asset.market_cap
            market_cap[asset.symbol] = asset.market_cap
            # Set equally weighted allocations
            equal_w[asset.symbol] = 1 / len(assets)

    # Capitalization Weighted allocations
    cap_w = {}
    for asset, cap in market_cap.items():
        cap_w[asset] = cap / total_cap

    risk_on = cube.risk_tolerance * 0.1
    risk_off = 1 - risk_on

    risk_w = {}
    for asset in cap_w.keys():
        rw = cap_w[asset] * risk_off + equal_w[asset] * risk_on
        risk_w[asset] = rw

    return key_to_id(risk_w)


def set_asset_allocation(cube, asset_id, percent):
    asset = AssetAllocation.query.filter_by(
        cube_id=cube.id,
        currency_id=asset_id
        ).first()
    if asset:
        asset.percent = percent
        asset.reserved_base = None
        asset.reserved_quote = None
    # New Asset
    else:
        asset = AssetAllocation(
            cube_id=cube.id, 
            currency_id=asset_id,
            percent=percent,
            reserved_base=None,
            reserved_quote=None
            )
    db_session.add(asset)
    db_session.commit()

def set_allocations_to_index(cube, min_per_alloc=50):
    assets, port_bal = asset_composition(cube, min_per_alloc)
    asset_allocs = create_asset_allocs(assets, cube)
    
    # Update database with new asset allocations
    # Loop through asset allocations
    for asset_id, dec in asset_allocs.items():           
        percent = float(dec)
        set_asset_allocation(cube, asset_id, percent)
    # Update balances in accordance with asset allocations
    # Dictionary of balances with id as key
    bals = {}
    for bal in cube.balances:
        bals[bal.currency_id] = bal.total
    # Check for missing balances vs. assets
    for asset_id in asset_allocs.keys():
        if asset_id not in bals.keys():
            log.debug(asset_id)
            # Add balance for each exchange connection
            for ex_name in cube.connections:
                log.debug(ex_name)
                if not cube.connections[ex_name].failed_at:
                    ex_id = cube.connections[ex_name].exchange_id
                    # Check to see if asset occurs in an ex_pair
                    ex_pair = ExPair.query.filter(ExPair.exchange_id == ex_id,
                                                 or_(ExPair.quote_currency_id == asset_id,
                                                    ExPair.base_currency_id == asset_id
                                                    )
                                                 ).first()
                    log.debug(ex_pair)
                    # Add missing ex_pair balance and set to 0
                    if ex_pair:
                        bal = Balance(
                            cube_id=cube.id,
                            currency_id=asset_id,
                            exchange_id=ex_id,
                            available=0,
                            total=0,
                            last=0
                            )
                        db_session.add(bal)
                        db_session.commit()

    # Set 0% allocations for balances not allocated
    # This will cause balances to sell out
    for asset_id in bals.keys():
        if asset_id not in asset_allocs.keys():
            set_asset_allocation(cube, asset_id, 0)

    # Update cube.reallocated_at
    cube.reallocated_at = datetime.utcnow()
    db_session.add(cube)
    db_session.commit()

    return True