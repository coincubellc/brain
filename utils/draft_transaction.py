from sqlalchemy.orm.exc import NoResultFound

from database import *
from tools import cur_ids_from_balances
from .api import get_price
from .order import bals_from_order



def draft_tx(cube, ex, order_id, bals):
    log.debug(f'{cube} Draft transaction {order_id}')
    
    # Deal with USD/USDT discrepency with cc/ccxt
    if cube.all_orders[order_id].ex_pair.quote_currency.symbol == 'USD':
        quote = cube.all_orders[order_id].ex_pair.quote_symbol
    else:
        quote = cube.all_orders[order_id].ex_pair.quote_currency.symbol
    base = cube.all_orders[order_id].ex_pair.base_currency.symbol
    # Draft tx
    tx = Transaction(
        cube=cube,
        user=cube.user,
        ex_pair=cube.all_orders[order_id].ex_pair,
        type=cube.all_orders[order_id].side,
        quote_balance=bals[quote]['total'],
        base_balance=bals[base]['total'],
        api_response=order_id,
        exchange_rate=cube.all_orders[order_id].avg_price
    )

    # Get db balances
    bal_base, bal_quote = bals_from_order(cube, ex.id, order_id)

    # Update balances
    bal_quote.total = bals[quote]['total']
    bal_quote.available = bals[quote]['available']
    bal_base.total = bals[base]['total']
    bal_base.available = bals[base]['available']

    db_session.add(bal_quote)
    db_session.add(bal_base)

    log.debug(f'{ex} {cube} committing transaction')
    db_session.add(tx)
    db_session.commit()  


def draft_convert_tx(cube, ex, bal):
    log.debug(f'{cube} Draft convert transaction {bal}')
    # Get an exchange pair involving currency
    cur_ids = cur_ids_from_balances(cube, ex)

    ex_pair = ExPair.query.filter_by(
        exchange_id=ex.id,
        active=True
    ).filter(or_(and_(
        ExPair.quote_currency_id == bal.currency.id,
        ExPair.base_currency_id.in_(cur_ids)
    ), and_(
        ExPair.base_currency_id == bal.currency.id,
        ExPair.quote_currency_id.in_(cur_ids)
    ))).first()
    if not ex_pair:
        log.warning(f'{ex} {cube} No active ExPair for {bal.symbol}')
        ex_pair = ExPair.query.filter_by(
            exchange_id=ex.id,
        ).filter(or_(and_(
            ExPair.quote_currency_id == bal.currency.id,
            ExPair.base_currency_id.in_(cur_ids)
        ), and_(
            ExPair.base_currency_id == bal.currency.id,
            ExPair.quote_currency_id.in_(cur_ids)
        ))).first()
        price = None
    else:
        # Get current exchange rate
        log.debug(f'{ex} {cube} Getting last trade price (API)')
        if cube.exchange.name not in ['External', 'Manual']:
            try:
                price = get_price(
                            ex_pair.exchange.name, 
                            ex_pair.base_currency.symbol, 
                            ex_pair.quote_currency.symbol
                            )
                log.debug(price)
            except:
                try:
                    price = ex_pair.get_close()
                except:
                    price = None
        else:
            try:
                price = ex_pair.get_close()
            except:
                price = None

    # Draft convert tx
    tx = Transaction(
        cube=cube,
        user=cube.user,
        ex_pair=ex_pair,
        type='convert',
        api_response=None,
        exchange_rate=price
    )
    # Needed so that the order ID is added to the transaction
    # Without order ID, first time deposits will have 0 amounts
    db_session.add(tx)
    db_session.flush()
    # Adjust balances
    if ex_pair.quote_currency == bal.currency:
        tx.quote_balance = bal.total
        tx.base_balance = Balance.query.filter_by(
            cube_id=cube.id,
            currency_id=ex_pair.base_currency_id,
            exchange_id=ex.id
        ).one().total
    else:
        tx.base_balance = bal.total
        tx.quote_balance = Balance.query.filter_by(
            cube_id=cube.id,
            currency_id=ex_pair.quote_currency_id,
            exchange_id=ex.id
        ).one().total
    log.debug(f'{cube} convert tx {tx}')
    db_session.add(tx)
    db_session.commit()


def draft_deposit_tx(cube, ex, cur_id, cur_ids, new_bals):
    log.debug(f'{cube} Draft deposit transaction cur_id: {cur_id}')
    # Find suitable ex_pair
    # WARNING: assumes conjugate balance available (even if 0)
    ex_pair = ExPair.query.filter_by(
            exchange_id=ex.id,
            active=True
        ).filter(or_(and_(
            ExPair.quote_currency_id == cur_id,
            ExPair.base_currency_id.in_(cur_ids)
        ), and_(
            ExPair.base_currency_id == cur_id,
            ExPair.quote_currency_id.in_(cur_ids)
        ))).first()

    # Get current exchange rate
    log.debug(f'{ex} {cube} Getting last trade price (API)')
    if cube.exchange.name not in ['External', 'Manual']:
        try:
            price = get_price(
                        ex_pair.exchange.name, 
                        ex_pair.base_currency.symbol, 
                        ex_pair.quote_currency.symbol
                        )
            log.debug(price)
        except:
            try:
                price = ex_pair.get_close()
            except:
                price = None
    else:
        try:
            price = ex_pair.get_close()
        except:
            price = None

    # Draft deposit tx
    tx = Transaction(
        cube=cube,
        user=cube.user,
        ex_pair=ex_pair,
        type='deposit',
        api_response=None,
        exchange_rate=price
    )
    # Needed so that the order ID is added to the transaction
    # Without order ID, first time deposits will have 0 amounts
    log.debug(f'{cube} Drafting deposit transaction')
    db_session.add(tx)
    db_session.flush()
    log.debug(tx)
    if ex_pair.quote_currency_id == cur_id:
        tx.quote_balance = new_bals[cur_id]['total']
        try:
            tx.base_balance = Balance.query.filter_by(
                cube_id=cube.id,
                currency_id=ex_pair.base_currency_id,
                exchange_id=ex.id
            ).one().total
        except NoResultFound:
            # conjugate balance is new as well
            tx.base_balance = new_bals[ex_pair.base_currency_id]['total']
    else:
        tx.base_balance = new_bals[cur_id]['total']
        try:
            tx.quote_balance = Balance.query.filter_by(
                cube_id=cube.id,
                currency_id=ex_pair.quote_currency_id,
                exchange_id=ex.id
            ).one().total
        except NoResultFound:
            # conjugate balance is new as well
            tx.quote_balance = new_bals[ex_pair.quote_currency_id]['total']
    log.debug(f'{ex} {cube} committing deposit transaction')
    db_session.add(tx)
    db_session.commit()