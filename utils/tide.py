from database import *

DUST_AMOUNT = 9e-8
OUT_THRES_PRICE_PCT = 0.01
MAX_VAL = 0.25  # BTC
MIN_VAL = 0.005


def clamp_thresh(cube):
    # Clamp threshold at 5%
    if cube.threshold < 5:
        threshold = 0.05
    else:
        threshold = cube.threshold / 100
    return threshold


def calc_diff(comb):
    comb = comb.copy()
    # WARNING: everything nan if val=0
    comb['pct'] = comb['val'] / comb['val'].sum()
    comb['pct_diff'] = comb['pct'] - comb['pct_tgt']
    comb['pct_diff_pct'] = comb['pct_diff'] / comb['pct_tgt']
    comb['pct_diff_pct'] = comb['pct_diff_pct'].fillna(0)
    # for unallocated currencies that are below min val for trading,
    # set pct_diff_pct to 0 so it can go in_thres...
    comb.loc[((comb['pct_tgt'] == 0) & (comb['bal'] <= DUST_AMT)),
             'pct_diff_pct'] = 0
    comb['val_tgt'] = comb['val'].sum() * comb['pct_tgt']
    comb['val_diff'] = comb['val'] - comb['val_tgt']

    return comb


def out_thresh(cube, comb, exs):
    # comb_orders is list of tuples (val_cur_val, val_cur_price, ex_pairs, side)
    # where ex_pairs is tuple of (ex_pair, inverted)
    # where inverted denotes whether base/quote (buy/sell value/amount) need to be flipped
    comb = comb.copy()
    # draft buys before sells to prevent insufficient fund issues in trader
    # this also maximizes currencies balanced
    comb = comb.sort_values('pct_diff_pct', ascending=False)
    log.debug(f'{cube} Running out-threshold')
    threshold = clamp_thresh(cube)
    within_thresh = True
    comb_orders = []
    for cur_id, c in comb.iterrows():
        if c.cur == cube.val_cur:
            # valuation currency will be balanced once others are
            continue
        if abs(c.pct_diff_pct) < threshold:
            # currency within threshold
            continue

        if c.pct_tgt:
            # only set out_thres status for allocated currencies
            within_thresh = False

        val = abs(c.val_diff)
        price = c.price
        eps = get_ex_pairs(exs, c.cur, cube.val_cur)
        if not eps:
            continue
        if val >= MAX_VAL * len(eps):
            # prevent excessively large orders
            # warning: not checking if residual would satisfy trade minimums..
            val = MAX_VAL
        if c.val_diff < 0:
            side = 'buy'
            price *= 1 + OUT_THRES_PRICE_PCT
        else:
            side = 'sell'
            price *= 1 - OUT_THRES_PRICE_PCT
        comb_orders.append((val, price, eps, side))
        # adjust value by full difference
        # note: not an accurate representation
        # but this shows potential end-state
        # with everything within threshold
        comb.loc[cur_id, 'val'] -= c.val_diff
        comb.loc[cube.val_cur.id, 'val'] += c.val_diff
    comb = calc_diff(comb)
    return comb, comb_orders, within_thresh


def in_thresh(cube, comb, exs):
    # comb_orders is list of tuples (val_cur_val, val_cur_price, ex_pairs, side)
    # where ex_pairs is tuple of (ex_pair, inverted)
    # where inverted denotes whether base/quote (buy/sell value/amount) need to be flipped

    threshold = clamp_thresh(cube)

    comb = comb.copy()
    comb['lo_val'] = comb['val_tgt'] * float(1 - threshold)
    comb['hi_val'] = comb['val_tgt'] * float(1 + threshold)
    comb['tide_bal'] = comb['val'] / comb['price']
    comb['buy_price'] = comb['lo_val'] / comb['tide_bal']
    comb['sell_price'] = comb['hi_val'] / comb['tide_bal']
    comb['buy_val'] = comb['lo_val']
    comb['sell_val'] = comb['hi_val']

    comb_orders = []
    for cur_id, c in comb.iterrows():
        if c.cur == cube.val_cur:
            # valuation currency will be balanced once others are
            continue
        if not c.pct_tgt:
            # skip unallocated currency
            log.debug(f'{cube} in_thres: skipping unallocated {c.cur}')
            continue
        if c.pct_tgt == 1:
            # 100% single allocation means no in_thres
            log.debug(f'{cube} in_thres: skipping single allocation {c.cur}')
            continue
        ex_pairs = get_ex_pairs(exs, c.cur, inverts=False)
        if not ex_pairs:
            # assumption is this is a fiat currency. pair with val_cur and invert.
            log.debug(f'{cube} in_thres: trying {c.cur}/{cube.val_cur} (inverted)')
            ex_pairs = get_ex_pairs(exs, c.cur, cube.val_cur, inverts=True)
        eps = []
        for ep in ex_pairs:
            # only include allocated conjugate currencies
            if (ep[0].quote_currency.symbol in cube.allocations and
                    cube.allocations[ep[0].quote_currency.symbol].percent):
                eps.append(ep)
            else:
                log.debug(f'{cube} in_thres: skipping unallocated quote {ep[0].quote_currency}')

        if not eps:
            continue
        comb_orders.append((c.buy_val, c.buy_price, eps, 'buy'))
        comb_orders.append((c.sell_val, c.sell_price, eps, 'sell'))
    return comb, comb_orders


def rebalance_orders(cube, indiv, comb, comb_orders, orders):
    log.debug(f'{cube} Creating orders')

    for val, price, ex_pairs, side in comb_orders:
        log.debug(f'{cube} {(val, price, ex_pairs, side)}')

        remaining_val = val
        remaining_eps = len(ex_pairs)

        for ex_pair, inverted in ex_pairs:
            log.debug(f'{cube} {(ex_pair, inverted)}')
            # abort if no remaining val (clumped into earlier order)
            if not remaining_val or not remaining_eps:
                log.debug(f'{cube} No remaining value (skipping)')
                break

            # in_thres existing order checks
            open_orders = Order.query.filter_by(
                cube_id=cube.id,
                ex_pair_id=ex_pair.id,
            ).all()
            count = len(open_orders)
            if cube.connections[ex_pair.exchange.name].liquidation_currency:
                # liquidating. existing orders already marked as pending cancellation.
                # TODO: do here...?
                log.debug(f'{cube} Liquidating exchange')
                pass
            else:
                # THIS SHOULD NO LONGER HAPPEN SINCE ORDERS ARE CANCELLED
                if count == 2:
                    # existing buy/sell orders (in-thres)
                    # TODO: check if orders are actually buy and sell?
                    # or are still relevant?
                    log.debug(f'{cube} Existing {ex_pair} orders (skipping)')
                    # treat as proportionate order..
                    remaining_val -= remaining_val / remaining_eps
                    remaining_eps -= 1
                    continue
                if count == 1:
                    # either buy or sell order filled.
                    # cancel other and replace both.
                    log.debug(f'{cube} {open_orders[0]} [C]')
                    open_orders[0].pending = True

            b, q = ex_pair.base_currency, ex_pair.quote_currency
            # ensure both balances exist (possible to trade)
            # TODO: move check upstream. should not even be added to list
            try:
                qbal = indiv['bal'][q.id, ex_pair.exchange.id]
                bbal = indiv['bal'][b.id, ex_pair.exchange.id]
            except KeyError:
                log.debug(f'{cube} Not viable: {ex_pair}')
                # deduct from remaining eps
                remaining_eps -= 1
                continue

            v = remaining_val / remaining_eps
            r_eps = remaining_eps
            while (r_eps > 1) and (v < MIN_VAL):
                # boost value
                r_eps -= 1
                v = remaining_val / r_eps

            # price is val_cur per 1 X
            if inverted:
                # X is the quote
                # need to find p as X per 1 b
                p = 1 / price * comb['price'][b.id]
                v /= price
                a = v / p
                s = 'buy' if side == 'sell' else 'sell'
            else:
                p = price / comb['price'][q.id]
                a = v / price
                v = a * p
                s = side

            if s == 'buy':
                # qbal = indiv['bal'][q.id, ex_pair.exchange.id]
                if v > qbal:
                    v = qbal
                    a = v / p
            else:
                # bbal = indiv['bal'][b.id, ex_pair.exchange.id]
                if a > bbal:
                    a = bbal
                    v = a * p

            # TEMP fudge factor: trim 1sat from amount to avoid rounding issues
            # leaving v the same...
            a -= 1e-8

            val_cur_val = a * comb['price'][b.id]

            # slight sanity check
            if val_cur_val > val * 1.1:
                log.warning(f'{cube} Order val exceeds total ({val_cur_val} vs {val})')
                continue
            params = {
                'base': ex_pair.base_symbol,
                'quote': ex_pair.quote_symbol
            }
            d = api_request(cube,
                          'GET',
                          ex_pair.exchange.name,
                          '/details',
                          params
                          )
            if d and (a < d['min_amt']) or (a * p < d['min_val']):
                log.debug(f'{cube} Order minimum not met for {ex_pair} {s} (skipping)')
                remaining_eps -= 1
                continue

            if s == 'buy':
                indiv.loc[(q.id, ex_pair.exchange.id), 'bal'] -= v
            else:
                indiv.loc[(b.id, ex_pair.exchange.id), 'bal'] -= a

            orders.append((trunc(a), trunc(p), ex_pair.id, s))

            # deduct order value
            remaining_val -= val_cur_val
            # deduct eps combined to make order viable (meet min val)
            remaining_eps = r_eps - 1
    return orders