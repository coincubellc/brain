from database import *


def apply_cost_averaging(cube, indiv):

    # For ca in cas
    cost_averages = DollarCostAverage.query.filter_by(cube_id=cube.id).all()
    for ca in cost_averages:
        value = ca.value

        # Cap value of quote for 'buy' if necessary
        if ca.side == 'buy':
            if ca.value > indiv.loc[(ca.quote_currency_id, cube.exchange.id), 'bal']:
                value = indiv.loc[(ca.quote_currency_id, cube.exchange.id), 'bal']
            # Take value and find balance target
            amount = float(value) / indiv.loc[(ca.base_currency_id, cube.exchange.id), 'price']
            base_bal_tgt = indiv.loc[(ca.base_currency_id, cube.exchange.id), 'bal'] + float(amount)
            quote_bal_tgt = indiv.loc[(ca.quote_currency_id, cube.exchange.id), 'bal'] - float(value)

        # Cap base amount for 'sell' if necessary
        if ca.side == 'sell':
            # Take value and find balance target
            amount = float(value) / indiv.loc[(ca.base_currency_id, cube.exchange.id), 'price']
            if amount > indiv.loc[(ca.base_currency_id, cube.exchange.id), 'bal']:
                amount = indiv.loc[(ca.base_currency_id, cube.exchange.id), 'bal']
            base_bal_tgt = indiv.loc[(ca.base_currency_id, cube.exchange.id), 'bal'] - float(amount)
            quote_bal_tgt = indiv.loc[(ca.quote_currency_id, cube.exchange.id), 'bal'] + float(value)

        # Cap at 0
        if base_bal_tgt < 0:
            base_bal_tgt = 0
        if quote_bal_tgt < 0:
            quote_bal_tgt = 0

        # Set bal_tgt
        indiv.loc[(ca.base_currency_id, cube.exchange.id), 'bal_tgt'] = base_bal_tgt
        indiv.loc[(ca.quote_currency_id, cube.exchange.id), 'bal_tgt'] = quote_bal_tgt
    
    # Logging
    log.debug('%s Individual valuations\n%s' %
              (cube, indiv.reset_index().loc[:, ['ex_id', 'cur_id', 'val', 'val_nnls']]))

    set_target_balances(cube, indiv)

    return indiv


def set_target_balances(cube, indiv):
    # set balance targets
    for b in cube.balances:
        try:
            b.target = indiv['bal_tgt'][b.currency_id, b.exchange_id]
            # convert nan to None
            if b.target != b.target:
                b.target = None
        except KeyError:
            b.target = None

        db_session.add(b)

    db_session.add(cube)
    db_session.commit()