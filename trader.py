#!/usr/bin/env python3
from pprint import pformat
import http
from threading import Event
from celery import Celery, group, chain
from celery.exceptions import SoftTimeLimitExceeded
import settings

from tools import (sanity_check, calc_indiv, calc_comb, 
                   liquidate, update_cube_cache)
from utils.api import api_request, get_api_creds
from utils.allocations import set_allocations_to_index
from utils.order import (cancel_order, place_order, target_orders)
from utils.reconcile import reconcile_balances, reconcile_order
from utils.regression import regression
from utils.cost_averaging import apply_cost_averaging
from database import *
import numpy as np
# Replacing datetime.time (Do not move)
from time import time

CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = CELERY_BROKER_URL

celery = Celery('trader', backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)
celery.conf.broker_transport_options = {'fanout_prefix': True}
celery.conf.broker_transport_options = {'fanout_patterns': True}
celery.conf.worker_prefetch_multiplier = 1
celery.conf.task_time_limit = 1800
celery.conf.task_soft_time_limit = 12000
celery.conf.ONCE = settings.CELERY_ONCE

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)
stopevent = Event()


class SqlAlchemyTask(celery.Task):
    """An abstract Celery Task that ensures that the connection the the
    database is closed on task completion"""
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        db_session.close()
        db_session.remove()   

@celery.task(base=SqlAlchemyTask)
def place_orders(cube_id, orders):
    try:
        cube = Cube.query.get(cube_id)
        log.debug(f'{cube} Placing Orders')
        for order in orders:
            # Arguments: cube_id, ex_pair_id, side, amount, price
            place_order(cube_id, order[2], order[3], order[0], order[1])
        update_cube_cache(cube_id, False) 
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)  
        ## To do: error handling  

@celery.task(base=SqlAlchemyTask)
def cancel_orders(cube_id, ex, orders):
    try:
        cube = Cube.query.get(cube_id)
        log.debug(f'{cube} Canceling Orders')
        for order in orders:
            cancel_order(cube_id, 
                        ex.id, 
                        order['order_id'], 
                        order['base'],
                        order['quote']
                        )
        update_cube_cache(cube_id, False) 
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)  
        ## To do: error handling  

@celery.task(base=SqlAlchemyTask)
def unrecognized_activity(cube_id):
    try:
        # Possible theft of key and malicious trading activity
        # Delete all open orders and force update
        cube = Cube.query.get(cube_id)
        log.debug(f'{cube} Unrecognized Activity')
        for conn in cube.connections.values():
            ex = conn.exchange
            creds = get_api_creds(cube, ex)
            args = {**creds, **{'type': 'open'}}
            orders = api_request(cube, 'GET', ex.name, '/orders', args)
            if orders:
                cancel_orders.delay(cube_id, ex, orders)
        db_session.add(cube)
        db_session.commit()
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)  
        ## To do: error handling

@celery.task(base=SqlAlchemyTask)
def trigger_rebalance(cube_id):
    try:
        cube = Cube.query.get(cube_id)
        log.debug(f'{cube} Rebalance triggered')
        cube.reallocated_at = datetime.utcnow()
        for b in cube.balances:
            b.target = None
        db_session.add(cube)
        db_session.commit()
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)  
        ## To do: error handling

def get_start_timestamp(cube, db_tx):
    if db_tx:
        log.debug(f'{cube} Tranasactions exist, updating from {db_tx.timestamp}')
        return int(float(db_tx.timestamp)) + 1
    else:
        log.debug(f'{cube} Transactions do not exist, updating from account start')
        return int(datetime.timestamp(cube.created_at) * 1000) # Convert to milliseconds


def import_trades(cube, ex, creds, since):
    now = time() * 1000
    days = 10
    trades = pd.DataFrame()
    url = '/trades'

    if ex.name == 'Binance':
        while since < now:
            new_trades = pd.DataFrame()
            for bal in cube.balances:
                ex_pairs = ExPair.query.filter_by(
                            exchange_id=ex.id, active=True
                            ).filter(or_(
                                ExPair.base_currency_id == bal.currency_id,
                                ExPair.quote_currency_id == bal.currency_id,
                            )).all()
                for ex_pair in ex_pairs:
                    args = {**creds, 
                        **{
                            'base': ex_pair.base_symbol,
                            'quote': ex_pair.quote_symbol,
                            'limit': 1000,
                            'since': since
                        }
                    }
                    binance_trades = api_request(cube, 'GET', ex.name, url, args)
                    binance_trades = pd.read_json(binance_trades)
                    new_trades = new_trades.append(binance_trades)
            if not new_trades.empty:
                new_trades = new_trades.sort_index()
                new_trades.timestamp = new_trades.timestamp.astype(np.int64)//10**6
                since = new_trades.iloc[-1].timestamp + 1
                trades = trades.append(new_trades)
            elif since < now:
                since = since + 24 * 60 * 60 * days * 1000
            else:
                break

    else:
        while since < now:
            args = {**creds, **{'since': since}}
            new_trades = api_request(cube, 'GET', ex.name, url, args)
            new_trades = pd.read_json(new_trades)
            if not new_trades.empty:
                new_trades.timestamp = new_trades.timestamp.astype(np.int64)//10**6
                since = new_trades.iloc[-1].timestamp + 1
                trades = trades.append(new_trades)
            elif since < now:
                since = since + 24 * 60 * 60 * days * 1000
            else:
                break

    if not trades.empty:
        # Adjustments to dataframe to match table structure
        fee = trades['fee'].apply(pd.Series)
        try:
            fee = fee.drop(['type'], axis=1)
        except:
            pass
        try:
            fee = fee.rename(index=str, columns={'rate': 'fee_rate', 'cost': 'fee_amount', 'currency': 'fee_currency'})
        except:
            pass
        trades = pd.concat([trades, fee], axis=1)
        trades = trades.rename(index=str, columns={'id': 'tx_id', 'order': 'order_id', 'amount': 'base_amount', 'cost': 'quote_amount'})
        symbol = trades['symbol'].str.split('/', n=1, expand=True)
        trades['base_symbol'] = symbol[0]
        trades['quote_symbol'] = symbol[1]
        trades['trade_type'] = trades['type']
        trades['type'] = trades['side']
        trades.drop(['side', 'symbol', 'fee'], axis=1, inplace=True)
        _, i = np.unique(trades.columns, return_index=True)
        trades = trades.iloc[:, i]
        trades = trades.fillna(value=0)

        # Add trades to database
        log.debug(f'{cube} Writing trades to database')
        for index, row in trades.iterrows():
            ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        quote_symbol=row.quote_symbol,
                        base_symbol=row.base_symbol,
                        ).first()
            if not ex_pair:
                continue
            if not row.base_amount:
                continue
            trade = TransactionFull(
                    tx_id=row.tx_id,
                    datetime=index,
                    order_id=row.order_id,
                    type=row.type,
                    trade_type=row.trade_type,
                    price=row.price,
                    base_symbol=row.base_symbol,
                    quote_symbol=row.quote_symbol,
                    exchange=ex,
                    cube=cube,
                    user=cube.user,
                )
            db_session.add(trade)
            db_session.commit()
            db_session.refresh(trade)
            if row.type == 'buy':
                trade.base_amount = row.base_amount
                trade.quote_amount = -row.quote_amount
            elif row.type == 'sell':
                trade.base_amount = -row.base_amount
                trade.quote_amount = row.quote_amount                
            try:
                trade.fee_rate = row.fee_rate
                trade.fee_amount = row.fee_amount
                trade.fee_currency = row.fee_currency
            except:
                pass
            db_session.add(trade)
            db_session.commit()


def import_transactions(cube, ex, creds, since):
    now = time() * 1000
    trans = pd.DataFrame()
    url = '/transactions'

    while True:
        args = {**creds, **{'since': since}}
        new_trans = api_request(cube, 'GET', ex.name, url, args)
        if not new_trans:
            break
        new_trans = pd.read_json(new_trans)
        if not new_trans.empty:
            new_trans.timestamp = new_trans.timestamp.astype(np.int64)//10**6
            since = new_trans.iloc[-1].timestamp + 1
            trans = trans.append(new_trans)
        else:
            break   

    if not trans.empty:
        log.debug(f'{cube} Adjusting dataframe to match table structure')
        # Adjustments to dataframe to match table structure
        if trans['fee'].any():
            fee = trans['fee'].apply(pd.Series)
            try:
                fee = fee.rename(index=str, columns={'rate': 'fee_rate', 'cost': 'fee_amount'})
                trans = pd.concat([trans, fee], axis=1)
            except:
                log.debug(f'{cube} missing transaction fee information, skipping...')
            trans.drop(['fee'], axis=1, inplace=True)
        trans = trans.rename(index=str, columns={'id': 'tx_id', 'txid': 'order_id'})
        trans.drop(['status', 'updated', 'timestamp'], axis=1, inplace=True)
        _, i = np.unique(trans.columns, return_index=True)
        trans = trans.iloc[:, i]
        trans = trans.fillna(value=0)

        # Add transactions to database
        log.debug(f'{cube} Writing transactions to database')
        for index, row in trans.iterrows():
            log.debug(row)
            log.debug(row.currency)
            if not row.amount:
                continue
            cur = Currency.query.filter_by(symbol=row.currency).first()
            if not cur:
                continue
            if row.type not in ['deposit', 'withdrawal', 'withdraw']:
                continue
            if row.type == 'withdraw':
                t_type = 'withdrawal'
            else:
                t_type = row.type

            ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        base_currency_id=cur.id,
                        ).first()
            if t_type == 'deposit':
                base_amount = row.amount
            elif t_type == 'withdrawal':
                base_amount = -row.amount
            quote_amount = 0
            if not ex_pair:
                ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        quote_currency_id=cur.id,
                        ).first()
                base_amount = 0
                if t_type == 'deposit':
                    quote_amount = row.amount
                elif t_type == 'withdrawal':
                    quote_amount = -row.amount

            tx = TransactionFull(
                    tx_id=row.tx_id,
                    datetime=index,
                    order_id=row.order_id,
                    tag=row.tag,
                    type=t_type,
                    base_symbol=ex_pair.base_currency.symbol,
                    quote_symbol=ex_pair.quote_currency.symbol,
                    exchange=ex,
                    cube=cube,
                    user=cube.user,
                )
            print(tx)
            print(base_amount)
            print(quote_amount)
            db_session.add(tx)
            db_session.commit()
            db_session.refresh(tx)
            tx.base_amount = base_amount
            tx.quote_amount = quote_amount
            db_session.add(tx)
            db_session.commit()


def update_transactions(cube, creds):
    log.debug(f'{cube} Updating transactions')
    db_tx = TransactionFull.query.filter_by(
                    cube_id=cube.id
                    ).order_by(
                    TransactionFull.id.desc()
                    ).first()

    try:
        ts = get_start_timestamp(cube, db_tx)
        log.debug(f'{cube} Get Transactions')
        import_transactions(cube, cube.exchange, creds, ts)
        log.debug(f'{cube} Get Trades')
        import_trades(cube, cube.exchange, creds, ts)
    except Exception as e:
        log.debug(e)
        return False
        
    return True


def order_reconciliation(cube, ex, creds, bals):
    # Reconcile db orders
    log.debug(f'{cube} Reconciling database orders (API)')
    if cube.orders:
        cube_orders = cube.orders.copy()
        for order in cube_orders:
            if order.ex_pair.exchange.name != ex.name:
                # order for different exchange
                continue
            # Get order from exchange
            url = f'/order/{order.order_id}'
            quote_symbol = order.ex_pair.quote_currency.symbol
            base_symbol = order.ex_pair.base_currency.symbol
            args = {**creds, **{'base': base_symbol, 'quote': quote_symbol}}
            ex_order = api_request(cube, 'GET', ex.name, url, args)
            if ex_order and ex_order != 'InvalidOrder':
                # Reconcile order
                reconcile_order(cube, ex, order.order_id, ex_order, bals)
            # Cancel oustanding order
            cancel_order(
                cube.id, 
                ex.id, 
                order.order_id, 
                base_symbol, 
                quote_symbol
                ) 

    if ex.name not in ['External', 'Manual', 'Binance']:
        # Get api orders
        log.debug(f'{cube} Checking for rogue orders (API)')
        args = {**creds, **{'type': 'open'}}
        orders = api_request(cube, 'GET', ex.name, '/orders', args)
        # Cancel outstanding rogue orders
        if orders:
            log.debug(f'{cube} Rogue orders {orders}')
            # Reconcile exchange orders to known orders
            for order_id in orders:
                log.debug(f'Order ID {order_id}')
                # Cancel rogue orders and set unrecognized activity flag
                if order_id not in cube.all_orders and cube.algorithm.name != "Tracker":
                    log.info(f'{cube} Canceling order: {order_id} (rogue)')
                    cancel_order(cube.id, ex.id, order_id)
            cube.unrecognized_activity = True
            db_session.add(cube)
            db_session.commit()

    if ex.name == 'Binance':
        # Check for balance available/total mismatches
        for bal in cube.balances:
            if bal.total > bal.available:
                # Part of balance has been reserved due to an open trade
                ex_pairs = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        active=True
                    ).filter(or_(
                        ExPair.base_currency_id == bal.currency_id,
                        ExPair.quote_currency_id == bal.currency_id,
                    )).all()
                # Check all possible pairs
                log.debug(f'{cube} Checking for rogue orders (API)')
                for ex_pair in ex_pairs:
                    quote_symbol = ex_pair.quote_currency.symbol
                    base_symbol = ex_pair.base_currency.symbol
                    args = {**creds, **{'base': base_symbol, 'quote': quote_symbol, 'type': 'open'}}
                    orders = api_request(cube, 'GET', ex.name, '/orders', args)
                    if orders:
                        for order_id in orders:
                            log.info(f'{cube} Canceling order: {order_id} (rogue)')
                            cancel_order(cube.id, ex.id, order_id, base_symbol, quote_symbol)
                        cube.unrecognized_activity = True
                        db_session.add(cube)
                        db_session.commit()                          


def set_last(cube):
    for bal in cube.balances:
        # Set last balance to current total
        bal.last = bal.total
        db_session.add(bal)
    db_session.commit()


@celery.task(base=SqlAlchemyTask)
def reconcile_cube(cube_id):
    try:
        cube = Cube.query.get(cube_id)
        # Reconcile cube
        for conn in cube.connections.values():
            ex = conn.exchange
            creds = get_api_creds(cube, ex)
            log.info(f'{cube} Reconciling {ex}')

            # Set last balance to total
            set_last(cube)

            # Get api balances
            log.debug(f'{cube} Getting balances (API)')
            bals = api_request(cube, 'GET', ex.name, '/balances', creds)

            if bals:
                # Reconcile orders
                order_reconciliation(cube, ex, creds, bals)
                # Reconcile exchange balances with db balances
                # Covers rogue orders, deposits, etc.
                log.debug(f'{cube} Reconciling Balances')
                reconcile_balances(cube, ex, bals)

            # Test new full transactions 
            # if cube.user_id == 2:
            #     log.debug(f'{cube} Updating full transactions (test user 2)')
            #     update_transactions(cube, creds)

        if cube.algorithm.name == "Tracker":
            cube.suspended_at = datetime.utcnow()
            db_session.add(cube)
            db_session.commit()
            # Remove from CubeCache
            update_cube_cache(cube_id, False)
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)
    # except http.client.RemoteDisconnected:
    #     reconcile_cube(cube_id)
        ## To do: error handling

def rebalance(cube, indiv, comb):
    log.debug(f'{cube} Rebalancing')
    r = False
    # Run optimization if needed
    if cube.trading_status == 'off':
        log.debug(f'{cube} Trading off (skipping)')
        r = False
    elif comb.empty:
        log.debug(f'{cube} Liquidating single exchange (skipping)')
        r = False
    else:  
        if not cube.balanced_at:
            # Not previously balanced
            log.debug(f'{cube} Not previously balanced (running optimization)')
            cube.reallocated_at = datetime.utcnow()
            r = True
        elif (cube.reallocated_at and
            cube.auto_rebalance and
            (cube.reallocated_at >= cube.balanced_at)):
            # Recently reallocated. Need to rebalance
            log.debug(f'{cube} Recently reallocated (running optimization)')
            r = True
        elif (cube.algorithm.name == 'Index' and
            cube.rebalance_interval and
            cube.auto_rebalance):
            if not cube.reallocated_at:
                cube.reallocated_at = datetime.utcnow()
            if ((cube.reallocated_at +
                timedelta(seconds=(cube.rebalance_interval))) <= datetime.utcnow()):
                set_allocations_to_index(cube)
                log.debug(f'{cube} New rebalance interval for Index (running optimization)')
                cube.reallocated_at = datetime.utcnow()
                r = True

        elif cube.rebalance_interval and cube.auto_rebalance:
            if not cube.reallocated_at:
                cube.reallocated_at = datetime.utcnow()
            if ((cube.reallocated_at +
                timedelta(seconds=(cube.rebalance_interval))) <= datetime.utcnow()):
                log.debug(f'{cube} New rebalance interval for Centaur (running optimization)')
                cube.reallocated_at = datetime.utcnow()
                r = True
        else:
            log.info(f'{cube} Optimization complete')
    if r:
        log.info(f'{cube} Running Optimization')
        try:
            indiv, comb, transfer_details = regression(cube, indiv, comb)
            if indiv is None:  # just to be safe, should never happen
                log.info('No valid solution from regression.')
            if transfer_details:
                log.debug(transfer_details)
                # Decide how to display this info the users
                # Likely best option is to use a database table
                pass
            if not transfer_details:
                # Delete any existing transfer info from database
                # Or keep the data and have a boolean which
                # is marked for transfer success
                # probably better to keep transfer data than 
                # to delete for debugging purposes
                pass
        except:
            log.exception('Exception from regression function')

def cost_average(cube, indiv):
    log.debug(f'{cube} Cost averaging')
    if cube.trading_status == 'off':
        log.debug(f'{cube} Trading off (skipping)')
    elif not cube.balanced_at:
        log.debug(f'{cube} Not previously balanced (running cost averaging)')
        cube.reallocated_at = datetime.utcnow()
        apply_cost_averaging(cube, indiv)
    elif cube.rebalance_interval:
        log.debug(f'{cube} Rebalance interval set to {cube.rebalance_interval}')
        if not cube.reallocated_at:
            log.debug(f'{cube} Not previously reallocated (running cost averaging)')
            cube.reallocated_at = datetime.utcnow()
            apply_cost_averaging(cube, indiv)
        elif ((cube.reallocated_at + timedelta(seconds=(cube.rebalance_interval))) <= datetime.utcnow()):
            log.debug(f'{cube} Running cost averaging')
            cube.reallocated_at = datetime.utcnow()
            apply_cost_averaging(cube, indiv)
        elif cube.reallocated_at > cube.balanced_at:
            # Check for open orders
            for b in cube.balances:
                # If target orders, don't create new cost average trades
                if b.target != None:
                    return
            log.debug(f'{cube} Running cost averaging')
            apply_cost_averaging(cube, indiv)

@celery.task(base=SqlAlchemyTask)
def new_orders(cube_id):  
    try:
        cube = Cube.query.get(cube_id)
        log.info(f'{cube} Generating Orders')
        #### Sanity Check ####
        if not sanity_check(cube):
            log.warning(f'{cube} failed sanity check')
            update_cube_cache(cube_id, False) 
            return    
            
        #### Individual Valuations ####
        full_indiv = calc_indiv(cube)
        log.debug(f'{cube} Individual valuations:\n{full_indiv}')

        #### Combined Valuations ####
        # full_comb = calc_comb(cube, full_indiv)

        #### Liquidation #####
        # Generate liqiudation orders and remove from individual valuations
        indiv, liquidation_orders = liquidate(cube, full_indiv)
        log.debug(f'{cube} Liquidation Orders:\n{format(liquidation_orders)}')
        # Calculate live combined valuations from live individual valuations
        log.debug(f'{cube} Combined valuations')
        comb = calc_comb(cube, indiv)

        # Rebalance cubes
        if cube.algorithm.name in ['Index', 'Centaur', 'Optimized']:
            rebalance(cube, indiv, comb)

        # Cost Averaging
        if cube.algorithm.name == 'Sphinx':
            cost_average(cube, indiv)

        if cube.trading_status == 'live':
            #### Generate Target Allocation Orders ####
            orders = target_orders(cube, indiv, comb, orders=[])
            log.debug(f'{cube} Individual Orders:\n{pformat(orders)}')
            # Add back liquidation orders
            orders.extend(liquidation_orders)
            if orders:
                place_orders.delay(cube_id, orders)
            else:
                update_cube_cache(cube_id, False)
                cube.balanced_at = datetime.utcnow()  
                db_session.add(cube)
                db_session.commit()
                log.debug(f'{cube} No Orders')
            cube.suspended_at = datetime.utcnow()
            db_session.add(cube)
            db_session.commit()
        update_cube_cache(cube_id, False)
    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)
    # except http.client.RemoteDisconnected:
    #     new_orders(cube_id)
        ## To do: error handling

@celery.task(base=SqlAlchemyTask)
def process_cube(cube_id):
    try:
        cube = Cube.query.get(cube_id)
        cache = CubeCache.query.filter_by(cube_id=cube_id).first()
        log.debug(f'{cube} Processing')

        if cache and cache.processing == True:
            log.debug(f'{cube} already in cache')
            return
        else:
            log.debug(f'{cube} adding to cache')
            # Add to CubeCache
            update_cube_cache(cube_id, True)     

        #### Update Cube if Tracker ####
        if cube.algorithm.name == "Tracker":
            reconcile_cube.delay(cube_id)
        else:
            log.debug(f'{cube} reconcile/generate new orders')
            #### Reconcile Cube/Generate New Orders ####
            chain(reconcile_cube.si(cube_id), new_orders.si(cube_id))()

    except SoftTimeLimitExceeded:
        update_cube_cache(cube_id, False)
    # except http.client.RemoteDisconnected:
    #     process_cube(cube_id)

def run_trader():
    # Find active Cubes
    active = and_(
        Cube.closed_at == None,
        Cube.trading_status == 'live',
        Cube.algorithm.has(Algorithm.name.in_(['Index', 'Centaur', 'Optimized', 'Sphinx'])),
        not_(Cube.connections.any(Connection.failed_at != None)),
        Cube.connections.any()    
        )

    try:
        cubes = Cube.query.filter(
            active,
            ).order_by(
            Cube.suspended_at
            ).all()

        log.debug(cubes)
        for cube in cubes:
            try:   
                log.debug(f'{cube} Starting')
                cache = CubeCache.query.filter_by(cube_id=cube.id).first()
                if cache and cache.processing == True:
                    if (cache.updated_at and
                        (cube.updated_at +
                        timedelta(hours=1)) <= datetime.utcnow()):
                        update_cube_cache(cube.id, False)  
                        log.debug(f'{cube} 1 hours past last update (processing)')
                        process_cube.delay(cube.id)
                    else:
                        log.debug(f'{cube} Already processing (skipping)')
                        continue
                elif not cube.balanced_at:
                    log.debug(f'{cube} Never balanced (processing)')
                    process_cube.delay(cube.id)
                elif (cube.reallocated_at and
                    (cube.reallocated_at >= cube.balanced_at)):
                    log.debug(f'{cube} Recently reallocated (processing)')
                    process_cube.delay(cube.id)
                elif cube.orders:
                    log.debug(f'{cube} Open orders (processing)') 
                    process_cube.delay(cube.id)                   
                elif (cube.suspended_at and
                    (cube.suspended_at + timedelta(minutes=60)) > datetime.utcnow()):
                    log.debug(f'{cube} less than 60 minutes since last update (skipping)')
                    continue
                else:
                    log.debug(f'{cube} Scheduled run (processing)')
                    process_cube.delay(cube.id)
                
            except Exception as e:
                log.exception('Unhandled exception')

    except Exception as e:
        log.exception('Main thread exception')
    finally:
        db_session.close()


def run_tracker():
    # Find active Cubes
    active = and_(
        Cube.closed_at == None,
        Cube.algorithm.has(Algorithm.name.in_(['Tracker'])),
        not_(Cube.connections.any(Connection.failed_at != None)),
        Cube.connections.any()    
        )

    try:
        cubes = Cube.query.filter(
            active,
            ).order_by(
            Cube.suspended_at
            ).all()

        for cube in cubes:
            try:   
                log.debug(f'{cube} Starting')
                cache = CubeCache.query.filter_by(cube_id=cube.id).first()
                if cache and cache.processing == True:
                    if (cube.suspended_at and
                        (cube.suspended_at +
                        timedelta(hours=2)) <= datetime.utcnow()):
                        update_cube_cache(cube.id, False)  
                        log.debug(f'{cube} 2 hours past last update (processing)')
                        process_cube.delay(cube.id)
                    else:
                        log.debug(f'{cube} Already processing (skipping)')
                        continue
                elif (cube.suspended_at and
                    (cube.suspended_at + timedelta(seconds=(600))) > datetime.utcnow()):
                    log.debug(f'{cube} less than 5 minutes since last update (skipping)')
                    continue
                else:
                    log.debug(f'{cube} Scheduled run (processing)')
                    process_cube.delay(cube.id)
                
            except Exception as e:
                log.exception('Unhandled exception')

    except Exception as e:
        log.exception('Main thread exception')
    finally:
        db_session.close()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    run_trader()
    run_tracker()
